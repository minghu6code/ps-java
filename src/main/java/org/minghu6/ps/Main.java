package org.minghu6.ps;

import org.apache.commons.io.FileUtils;
import org.minghu6.ps.formatter.PSFormatter;
import org.minghu6.ps.interpreter.semantics.AnnotatedTree;
import org.minghu6.ps.interpreter.PSCompiler;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Option;
import picocli.CommandLine.IVersionProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;

@Command(name = "pslang", mixinStandardHelpOptions = true,
        versionProvider = Main.PropertiesVersionProvider.class,
        description = "Play a PlayScript file.",
        subcommands = {
                REPL.class,
                RUN.class,
                FMT.class
        }
)
public class Main implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        System.out.println("RUN!");
        return 0;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }

    /**
     * {@link IVersionProvider} implementation.
     */
    static class PropertiesVersionProvider implements IVersionProvider {
        public String[] getVersion() throws Exception {
            return new String[]{
                    getClass().getPackage().getImplementationVersion(),
            };
        }
    }

    static String readTextFile(File file) throws IOException {
        StringBuilder buffer = new StringBuilder();
        try (
            FileReader reader = new FileReader(file.getAbsolutePath());
            BufferedReader br = new BufferedReader(reader)
        ) {
            String line;
            while ((line = br.readLine()) != null) {
                buffer.append(line).append('\n');
            }
        }
        return buffer.toString();
    }
    enum ExitCode {
        Normal(0), IOFailed(1), CompileFailed(2);
        final int code;

        ExitCode(int code) {
            this.code = code;
        }
    }
}

@Command(name = "repl", description = "REPL",
        mixinStandardHelpOptions = true,
        versionProvider = Main.PropertiesVersionProvider.class
)
class REPL implements Callable<Integer> {
    @Override public Integer call() {
        System.out.println("REPL!");
        return 0;
    }

}

@Command(name = "fmt", description = "Formatter",
        mixinStandardHelpOptions = true,
        versionProvider = Main.PropertiesVersionProvider.class
)
class FMT implements Callable<Integer> {
    @Parameters(paramLabel = "SCRIPT", description = "pslang script file")
    File script;

    @Override public Integer call() throws IOException {
        PSFormatter psFormatter = new PSFormatter(FileUtils.readFileToString(script, StandardCharsets.UTF_8));
        psFormatter.fmt();
        return 0;
    }
}

@Command(name = "run", description = "RUN",
        mixinStandardHelpOptions = true,
        versionProvider = Main.PropertiesVersionProvider.class
)
class RUN implements Callable<Integer> {
    @Parameters(paramLabel = "SCRIPT", description = "pslang script file")
    File script;

    @Option(names = {"-S", "--symbols-dump"})
    boolean symbols_dump;

    @Option(names = {"-T", "--ast-dump"})
    boolean ast_dump;

    @Override public Integer call() {
        System.out.printf("read script: %s\n", script.getName());
        String scriptString;
        try {
            scriptString = Main.readTextFile(script);
        } catch (IOException ex) {
            System.err.println(ex.toString());
            return Main.ExitCode.IOFailed.code;
        }

        PSCompiler compiler = new PSCompiler();
        AnnotatedTree at = compiler.compile(scriptString, symbols_dump, ast_dump);

        compiler.dumpCompilationLogs();
        if (!at.hasCompilationError()) {
            Object result = compiler.Execute(at);
            System.out.println(result);
        } else {
            return Main.ExitCode.CompileFailed.code;
        }
        return Main.ExitCode.Normal.code;
    }
}
