package org.minghu6.ps.interpreter.semantics;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.minghu6.ps.interpreter.data.types.Function;
import org.minghu6.ps.interpreter.data.types.VoidType;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import java.util.stream.IntStream;

/**
 * 进行一些杂项的语义检查
 * <div>
 * <div> 01. break 只能出现在循环语句或case语句中</div>
 * <br/>
 * <p> 02.&nbsp&nbsp&nbsp&nbsp&nbsp return 语句</p>
 * <p> 02-01 函数声明了返回值，就一定要有return语句，除非返回值类型是void。</p>
 * <p> 02-02 类的构造函数里如果用到了return，不能带返回值。</p>
 * <p> 02-03 return语句只能出现在函数里。</p>
 * <p> 02-04 返回值类型检查（利用{@link Pass4TypeChecker}）</p>
 * <br/>
 * <p>03.左值（取的地址）</p>
 * <p>03-01 标注左值 （不标注就是右值）；</p>
 * <p>03-02 检查表达式能否生成合格的左值。</p>
 * <br/>
 * <p>04. 类的声明不能再函数里（TODO 未来应该也可以，只不过对生存期有要求）</p>
 * <br/>
 * <p>05. super()和this(), 只能是构造函数第一句（在{@link Pass3RefResolver}已经解决了）</p>
 * <br/>
 * </div>
 */
public class Pass5SomeSemanticValidator extends PSParserBaseListener {
    private AnnotatedTree at;

    public Pass5SomeSemanticValidator(AnnotatedTree at) {
        this.at = at;
    }

    @Override
    public void exitClassDeclaration(PSParserParser.ClassDeclarationContext ctx) {
        // 04 类的声明不能在函数里
        if (at.enclosingFunctionOfNode(ctx) != null){
            at.clog("can not declare class inside function", ctx);
        }
    }

    @Override
    public void exitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx) {
        // 02-01 函数定义了返回值，就一定要有相应的return语句。
        // TODO 更完善的是要进行控制流计算，不是仅仅有一个return语句就行了。
        if (ctx.typeTypeOrVoid() == null) {  // 排除构造器函数
            return;
        }
        if (at.typeOfNode.get(ctx.typeTypeOrVoid()) != VoidType.instance()
            && !checkFunctionReturnStatement(ctx)) {
            at.clog("return statement expected in function", ctx);
        }
    }

    @Override
    public void exitStatement(PSParserParser.StatementContext ctx) {

        if (ctx.RETURN() != null) {
            Function function = at.enclosingFunctionOfNode(ctx);
            if (function == null) {  // 02-03 return 只出现在函数里
                at.clog("return statement not in function body", ctx);
            } else if (function.isConstructor()) { // 02-02 类的构造函数不能有返回值
                at.clog("can not return a value from constructor", ctx);
            }
        }

        // 01. break
        else if (ctx.BREAK() != null) {
            if (!hasCorrectBreakPos(ctx)){
                at.clog("break statement not in loop or switch statements", ctx);
            }
        }
    }

    /**
     * 函数内部是否有return
     */
    private boolean checkFunctionReturnStatement(ParseTree ctx) {
        return
        IntStream.range(0, ctx.getChildCount()).anyMatch(i-> {
            ParseTree child = ctx.getChild(i);
            return
                (child instanceof PSParserParser.StatementContext
                && ((PSParserParser.StatementContext) child).RETURN() != null)

                        ||

                !(child instanceof PSParserParser.FunctionDeclarationContext
                    || child instanceof PSParserParser.ClassDeclarationContext)
                && checkFunctionReturnStatement(child)
                ;
        });
    }

    /**
     * break 只能出现在循环语句、switch-case语句里。
     */
    private boolean hasCorrectBreakPos(RuleContext ctx) {
        if (ctx.parent instanceof PSParserParser.StatementContext
                &&
            (((PSParserParser.StatementContext) ctx.parent).FOR() != null
                    || ((PSParserParser.StatementContext) ctx.parent).WHILE() != null)

                ||

             ctx.parent instanceof PSParserParser.SwitchBlockStatementGroupContext) {
            return true;
        } else if (ctx.parent == null
                || ctx.parent instanceof PSParserParser.FunctionDeclarationContext){
            return false;
        }
        else {
            return hasCorrectBreakPos(ctx.parent);
        }
    }
}
