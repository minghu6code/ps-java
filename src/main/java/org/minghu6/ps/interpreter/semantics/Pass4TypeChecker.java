package org.minghu6.ps.interpreter.semantics;

import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.PrimitiveType;
import org.minghu6.ps.interpreter.data.types.Type;
import org.minghu6.ps.interpreter.errors.IncompatiableTypeError;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import static java.lang.String.format;

/**
 * 第四遍扫描：类型检查
 *
 * <div>
 *     包括如下:
 *     <p>1. 赋值表达式</p>
 *     <p>2. 变量初始化</p>
 *     <p>3. 表达式里的运算是否类型匹配</p>
 *     <p>4. 返回值类型</p>
 * </div>
 */
public class Pass4TypeChecker extends PSParserBaseListener {
    private final AnnotatedTree at;

    public Pass4TypeChecker(AnnotatedTree at) {
        this.at = at;
    }

    @Override
    public void exitVariableDeclarator(PSParserParser.VariableDeclaratorContext ctx) {
        if (ctx.variableInitializer() != null) {
            Variable variable = (Variable) at.symbolOfNode.get(ctx.variableDeclaratorId());
            Type type1 = variable.type;
            Type type2 = at.typeOfNode.get(ctx.variableInitializer());
            checkAssign(type1, type2,
                    ctx,
                    ctx.variableDeclaratorId(), ctx.variableInitializer());
        }
    }

    @Override
    public void exitExpression(PSParserParser.ExpressionContext ctx) {
        if (ctx.bop != null && ctx.expression().size() >= 2) {
            Type type1 = at.typeOfNode.get(ctx.expression(0));
            Type type2 = at.typeOfNode.get(ctx.expression(1));

            switch (ctx.bop.getType()) {
                case PSParserParser.ADD:
                    //字符串能够跟任何对象做 + 运算
                    if (type1 != PrimitiveType.String && type2 != PrimitiveType.String){
                        checkNumericOperand(type1, ctx, ctx.expression(0));
                        checkNumericOperand(type2, ctx, ctx.expression(1));
                    }
                    break;
                case PSParserParser.SUB:
                case PSParserParser.MUL:
                case PSParserParser.DIV:
                case PSParserParser.LE:
                case PSParserParser.LT:
                case PSParserParser.GE:
                case PSParserParser.GT:
                    checkNumericOperand(type1, ctx, ctx.expression(0));
                    checkNumericOperand(type2, ctx, ctx.expression(1));
                    break;
                case PSParserParser.EQUAL:
                case PSParserParser.NOTEQUAL:

                    break;

                case PSParserParser.AND:
                case PSParserParser.OR:
                    checkBooleanOperand(type1, ctx, ctx.expression(0));
                    checkBooleanOperand(type2, ctx, ctx.expression(1));
                    break;

                case PSParserParser.ASSIGN:
                    checkAssign(type1,type2,ctx,ctx.expression(0),ctx.expression(1));
                    break;

                case PSParserParser.ADD_ASSIGN:
                case PSParserParser.SUB_ASSIGN:
                case PSParserParser.MUL_ASSIGN:
                case PSParserParser.DIV_ASSIGN:
                case PSParserParser.AND_ASSIGN:
                case PSParserParser.OR_ASSIGN:
                case PSParserParser.XOR_ASSIGN:
                case PSParserParser.MOD_ASSIGN:
                case PSParserParser.LSHIFT_ASSIGN:
                case PSParserParser.RSHIFT_ASSIGN:
                case PSParserParser.URSHIFT_ASSIGN:
                    if (PrimitiveType.isNumberType((PrimitiveType) type2)) {
                        checkAssign(
                                type1, type2,
                                ctx,
                                ctx.expression(0), ctx.expression(1));
                    }
                    else{
                        at.clog(
                            format("operand + %s should be numeric。", ctx.expression(1).getText()),
                            ctx
                        );
                    }

                    break;
            }
        }
    }

    /**
     * 检查赋值操作
     */
    private void checkAssign(Type type1, Type type2,
                             ParserRuleContext ctx,
                             ParserRuleContext operand1, ParserRuleContext operand2) {
        if (type2 instanceof PrimitiveType && type1 instanceof PrimitiveType
                && PrimitiveType.isNumberType((PrimitiveType) type2)) {
            try {
                PrimitiveType.getUpperType((PrimitiveType) type1, (PrimitiveType) type2);
            } catch (IncompatiableTypeError e) {
                at.clog(format("can't assign %s of type %s to %s of type %s",
                        operand2.getText(),
                        type2,
                        operand1.getText(),
                        type1),
                        ctx);
            }
        }  // else if instanceof Class / Function
        // 检查类的兼容性，函数的兼容性
    }

    /**
     * Check numeric operand
     */
    private void checkNumericOperand(
            Type type,
            PSParserParser.ExpressionContext exp,
            PSParserParser.ExpressionContext operand) {
        if (!PrimitiveType.isNumberType((PrimitiveType) type)) {
            at.clog(format("operand for arithmetic operation should be numeric : %s",
                    operand.getText()),
                    exp);
        }
    }

    /**
     * 检查类型是不是Boolean
     */
    private void checkBooleanOperand(
            Type type,
            PSParserParser.ExpressionContext exp,
            PSParserParser.ExpressionContext operand) {
        if (type != PrimitiveType.Boolean) {
            at.clog(format(
                    "operand for logical operation should be boolean : %s",
                    operand.getText()),
                    exp);
        }
    }
}
