package org.minghu6.ps.interpreter.semantics;

import lombok.Getter;
import lombok.Setter;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.minghu6.ps.interpreter.runtime.RuntimeLog;
import org.minghu6.ps.interpreter.syntax.PSParserParser;
import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.data.scopes.NameSpace;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.types.Function;
import org.minghu6.ps.interpreter.data.types.Type;

import org.minghu6.ps.interpreter.AbstractLog.LogLevel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.lang.String.format;

/**
 * 注解树，用于存放语义分析的结果
 */
public class AnnotatedTree {
    // AST : 抽象语法树
    public ParseTree ast = null;

    // Parsed Types : Primitive Type | Class | Function | Array | Enum | Method
    public List<Type> types = new LinkedList<>();

    // Node => Symbol
    public Map<ParserRuleContext, Symbol> symbolOfNode = new HashMap<>();

    // Node => Scope, exp: for, function create new scope
    public Map<ParserRuleContext, Scope> scopeOfNode = new HashMap<>();

    // Node => type(type inference)
    public Map<ParserRuleContext, Type> typeOfNode = new HashMap<>();

    // Global namespace
    @Getter
    @Setter
    NameSpace nameSpace = null;

    // 语义分析过程中生成的信息：info, warning, error
    public List<CompilationLog> compilationLogs = new LinkedList<>();
    public List<RuntimeLog> runtimeLogs = new LinkedList<>();

    // 构造函数里引用的 this
    public Map<Function, Function> thisConstructorRef = new HashMap<>();

    // 构造函数里引用的 super
    public Map<Function, Function> superConstructorRef = new HashMap<>();

    public AnnotatedTree() {}

    public void clog(String message, LogLevel level, ParserRuleContext ctx) {
        CompilationLog log = CompilationLog
                .builder()
                .ctx(ctx)
                .message(message)
                .ln(ctx.getStart().getLine())
                .col(ctx.getStart().getCharPositionInLine() + 1)
                .level(level)
                .build();

        compilationLogs.add(log);

        // System.out.println(log);
    }

    public void clog(String message, ParserRuleContext ctx) {
        this.clog(message, LogLevel.ERROR, ctx);
    }

    public void rlog(String message, LogLevel level, ParserRuleContext ctx) {
        RuntimeLog log = RuntimeLog
                .builder()
                .ctx(ctx)
                .message(message)
                .ln(ctx.getStart().getLine())
                .col(ctx.getStart().getCharPositionInLine() + 1)
                .level(level)
                .build();

        runtimeLogs.add(log);

        // System.out.println(log);
    }

    public void rlog(String message, ParserRuleContext ctx) {
        this.rlog(message, LogLevel.ERROR, ctx);
    }

    public boolean hasCompilationError() {
        return compilationLogs.stream().anyMatch(log -> log.level == LogLevel.ERROR);
    }

    public Variable lookupVariable(Scope scope, String idName) {
        Variable rtn = scope.getVariable(idName);

        if (rtn == null && scope.getEnclosingScope() != null) {
            rtn = lookupVariable(scope.getEnclosingScope(), idName);
        }

        return rtn;
    }

    public Class lookupClass(Scope scope, String idName) {
        Class rtn = scope.getClass(idName);

        if (rtn == null && scope.getEnclosingScope() != null) {
            rtn = lookupClass(scope.getEnclosingScope(), idName);
        }

        return rtn;
    }

    public Type lookupType(String idName) {
        return types.stream()
                    .filter(t -> t.getName().equals(idName))
                    .findFirst()
                    .orElse(null);
    }

    public Function lookupFunction(Scope scope, String idName, List<Type> paramTypes) {
        Function rtn = scope.getFunction(idName, paramTypes);

        if (rtn == null && scope.getEnclosingScope() != null) {
            rtn = lookupFunction(scope.getEnclosingScope(), idName, paramTypes);
        }
        return rtn;
    }

    public Variable lookupFVariable(Scope scope, String idName, List<Type> paramTypes) {
        Variable rtn = scope.getFVariable(idName, paramTypes);

        if (rtn == null && scope.getEnclosingScope() != null) {
            rtn = lookupFVariable(scope.getEnclosingScope(), idName, paramTypes);
        }

        return rtn;
    }

    /**
     * 逐级查找函数（或方法）。仅通过名字查找。如果有重名的，返回第一个就算了。//TODO 未来应该报警。
     */
    public Function lookupFunction(Scope scope, String name){
        Function rtn = null;
        if (scope instanceof Class){
            rtn = getMethodOnlyByName((Class)scope, name);
        }
        else {
            rtn = getFunctionOnlyByName(scope, name);
        }

        if (rtn == null && scope.getEnclosingScope() != null){
            rtn = lookupFunction(scope.getEnclosingScope(),name);
        }

        return rtn;
    }

    //对于类，需要连父类也查找
    private Function getMethodOnlyByName(Class theClass, String name){
        Function rtn = getFunctionOnlyByName(theClass, name);

        if (rtn == null && theClass.getParentClass() != null){
            rtn = getMethodOnlyByName(theClass.getParentClass(), name);
        }

        return rtn;
    }

    private Function getFunctionOnlyByName(Scope scope, String name){
        return (Function) scope.getSymbols()
                .stream()
                .filter(s -> s instanceof Function && s.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    /**
     * 查找节点所在Scope
     * 算法：逐级查找父节点，找到一个对应着Scope的上级节点
     */
    public Scope enclosingScopeOfNode(ParserRuleContext node) {
        Scope rtn = null;
        ParserRuleContext parent = node.getParent();
        if (parent != null) {
            rtn = scopeOfNode.get(parent);
            if (rtn == null) {
                rtn = enclosingScopeOfNode(parent);
            }
        }

        return rtn;
    }

    /**
     * 包含某结点的函数
     */
    public Function enclosingFunctionOfNode(RuleContext ctx){
        if (ctx.parent instanceof PSParserParser.FunctionDeclarationContext){
            return (Function) scopeOfNode.get(ctx.parent);
        }
        else if (ctx.parent == null){
            return null;
        }
        else return enclosingFunctionOfNode(ctx.parent);
    }

    /**
     * 包含某结点的类
     */
    public Class enclosingClassOfNode(RuleContext ctx) {
        if (ctx.parent instanceof PSParserParser.ClassDeclarationContext) {
            return (Class) scopeOfNode.get(ctx.parent);
        } else if (ctx.parent == null) {
            return null;
        }
        return enclosingClassOfNode(ctx.parent);
    }

    /**
     * 输出Scope中的内容，包括每个变量的名称、类型。
     */
//    public String getScopeTreeString() {
//        return scopeToString(nameSpace, "");
//    }
//
//    private String scopeToString(Scope scope, String indent) {
//        if (scope == null) return "";
//
//        return Stream.concat(
//            Stream.builder()
//                .add(indent)
//                .add(indent)
//                .add("\n")
//                .build(),
//            scope.symbols.stream().map(s -> {
//                if (s instanceof Scope) return scopeToString((Scope) s, indent + "\t");
//                else return format("%s\t%s\n", indent, s);
//            }))
//                .map(s -> (String) s )
//                .collect(Collectors.joining());
//
//    }
    public String getScopeTreeString(){
        StringBuffer sb = new StringBuffer();
        scopeToString(sb, nameSpace, "");
        return sb.toString();
    }

    private void scopeToString(StringBuffer sb, Scope scope, String indent) {
        sb.append(indent).append(scope).append('\n');
        for (Symbol symbol : scope.symbols) {
            if (symbol instanceof Scope) {
                scopeToString(sb, (Scope) symbol, indent + "\t");
            } else {
                sb.append(indent).append("\t").append(symbol).append('\n');
            }
        }
    }

    public void dumpCompilationLogs() {
        // 这应该不会被优化掉吧
        this.compilationLogs
                .stream()
                .peek(System.out::println)
                .collect(Collectors.toList());
    }

    public void dumpRuntimeLogs() {
        this.runtimeLogs
                .stream()
                .peek(System.out::println)
                .collect(Collectors.toList());
    }

    public void dumpLogs() {
        dumpCompilationLogs();
        dumpRuntimeLogs();
    }
}
