package org.minghu6.ps.interpreter.semantics;

import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.*;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import static java.lang.String.format;
/**
 * 第二遍扫描：解析变量、类继承、函数声明的类型。
 * 处理类成员变量和函数的参数。<br/>
 */
public class Pass2TypeResolver extends PSParserBaseListener {
    private final AnnotatedTree at;
    // 是否把本地变量加入符号表
    private boolean enterLocalVariable = false;


    public Pass2TypeResolver(AnnotatedTree at, boolean enterLocalVariable) {
        this.at = at;
        this.enterLocalVariable = enterLocalVariable;
    }

    public Pass2TypeResolver(AnnotatedTree at) {
        this.at = at;
    }

    /**
     * 为所声明的变量设置类型 (typeType)
     */
    @Override
    public void exitVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx) {
        Scope scope = at.enclosingScopeOfNode(ctx);  // 节点(ctx)对应的scope

        if (scope instanceof Class || enterLocalVariable) {
            Type type = at.typeOfNode.get(ctx.typeType());

            ctx.variableDeclarator().forEach(child -> {
                Variable variable = (Variable) at.symbolOfNode.get(
                        child.variableDeclaratorId()
                );
                variable.type = type;
            });
        }
    }

    @Override
    public void enterVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext ctx) {
        Scope scope = at.enclosingScopeOfNode(ctx);

        if (scope instanceof Class
            || enterLocalVariable
            || ctx.parent instanceof PSParserParser.FormalParameterContext) {
            String idName = ctx.Identifier().getText();
            Variable variable = new Variable(idName, scope, ctx);

            // 变量查重
            if (Scope.getVariable(scope, idName) != null) {
                at.clog(format("Variable or parameter already Declared: %s", idName), ctx);
            }

            scope.addSymbol(variable);
            at.symbolOfNode.put(ctx, variable);
        }
    }

    /**
     * 设置函数的参数类型，参数已经在
     * {@link Pass2TypeResolver#enterVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext)
     * enterVariableDeclaratorId}
     * 作为变量声明了，现在设置类型
     */
    @Override
    public void exitFormalParameter(PSParserParser.FormalParameterContext ctx) {
        // 设置参数类型
        Type type = at.typeOfNode.get(ctx.typeType());
        Variable variable = (Variable) at.symbolOfNode.get(ctx.variableDeclaratorId());
        variable.type = type;

        Scope scope = at.enclosingScopeOfNode(ctx);
        if (scope instanceof Function) {  // 只有function才会使用FormalParameter
            ((Function) scope).parameters.add(variable);
        }
    }

    /**
     * 设置函数返回值类型
     */
    @Override
    public void exitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx) {
        Function function = (Function) at.scopeOfNode.get(ctx);
        if (ctx.typeTypeOrVoid() != null) {
            function.returnType = at.typeOfNode.get(ctx.typeTypeOrVoid());
        } // else TODO 如果是类的构建函数，返回应该是类？

        // 函数查重，检查名称和参数
        Scope scope = at.enclosingScopeOfNode(ctx);
        Function foundFun = Scope.getFunction(scope, function.getName(), function.getParamTypes());
        if (foundFun != null && foundFun != function) {
            at.clog(format("Function or Method already declared: %s", ctx.getText()), ctx);
        }
    }

    /**
     * 设置类的父类
     */
    @Override
    public void enterClassDeclaration(PSParserParser.ClassDeclarationContext ctx) {
        Class theClass = (Class) at.scopeOfNode.get(ctx);
        // 设置父类
        if (ctx.EXTENDS() != null) {
            String parentClassName = ctx.typeType().getText();
            Type type = at.lookupType(parentClassName);
            if (type instanceof Class) {
                theClass.setParentClass((Class) type);
            } else {
                at.clog(format("unknown class: %s", parentClassName), ctx);
            }
        }
    }

    @Override
    public void exitTypeTypeOrVoid(PSParserParser.TypeTypeOrVoidContext ctx) {
        if (ctx.VOID() != null) {
            at.typeOfNode.put(ctx, VoidType.instance());
        } else {
            at.typeOfNode.put(ctx, at.typeOfNode.get(ctx.typeType()));
        }
    }

    /**
     * See also {@link Pass2TypeResolver#enterClassOrInterfaceType(PSParserParser.ClassOrInterfaceTypeContext)}
     * , {@link Pass2TypeResolver#exitFunctionType(PSParserParser.FunctionTypeContext)}
     * , {@link Pass2TypeResolver#exitPrimitiveType(PSParserParser.PrimitiveTypeContext)}
     */
    @Override
    public void exitTypeType(PSParserParser.TypeTypeContext ctx) {
        // 将下级的属性标注在本级
        Type type;
        if (ctx.classOrInterfaceType() != null) {
            type = at.typeOfNode.get(ctx.classOrInterfaceType());
        } else if (ctx.functionType() != null) {
            type = at.typeOfNode.get(ctx.functionType());
        } else {  // ctx.primitiveType() != null
            type = at.typeOfNode.get(ctx.primitiveType());
        }
        at.typeOfNode.put(ctx, type);
    }

    @Override
    public void enterClassOrInterfaceType(PSParserParser.ClassOrInterfaceTypeContext ctx) {
        if (ctx.Identifier() != null) {
            at.typeOfNode.put(ctx, at.lookupClass(
                    at.enclosingScopeOfNode(ctx),
                    ctx.getText()  // idName
                    ));
        }
    }

    @Override
    public void exitFunctionType(PSParserParser.FunctionTypeContext ctx) {
        DefaultFunctionType functionType = new DefaultFunctionType();
        at.types.add(functionType);
        at.typeOfNode.put(ctx, functionType);
        functionType.returnType = at.typeOfNode.get(ctx.typeTypeOrVoid());

        if (ctx.typeList() != null) {
            ctx.typeList()
                    .typeType()
                    .forEach(ttc -> functionType.paramTypes.add(at.typeOfNode.get(ttc)));
        }
    }

    @Override
    public void exitPrimitiveType(PSParserParser.PrimitiveTypeContext ctx) {
        Type type = null;
        if (ctx.BOOLEAN() != null) {
            type = PrimitiveType.Boolean;
        } else if (ctx.INT() != null) {
            type = PrimitiveType.Integer;
        } else if (ctx.LONG() != null) {
            type = PrimitiveType.Long;
        } else if (ctx.FLOAT() != null) {
            type = PrimitiveType.Float;
        } else if (ctx.DOUBLE() != null) {
            type = PrimitiveType.Double;
        } else if (ctx.BYTE() != null) {
            type = PrimitiveType.Byte;
        } else if (ctx.SHORT() != null) {
            type = PrimitiveType.Short;
        } else if (ctx.CHAR() != null) {
            type = PrimitiveType.Char;
        }else if (ctx.STRING() != null) {
            type = PrimitiveType.String;
        }

        at.typeOfNode.put(ctx, type);
    }
}
