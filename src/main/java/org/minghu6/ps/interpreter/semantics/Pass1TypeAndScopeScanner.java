package org.minghu6.ps.interpreter.semantics;

import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;
import org.minghu6.ps.interpreter.errors.SemanticFatalError;
import org.minghu6.ps.interpreter.data.scopes.BlockScope;
import org.minghu6.ps.interpreter.data.scopes.NameSpace;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.data.types.Function;

import java.util.Optional;
import java.util.Stack;
import static java.lang.String.format;

/**
 * 第一遍扫描：初步识别出所有类型（下一步 {@link Pass2TypeResolver}）以及Scope。
 */
public class Pass1TypeAndScopeScanner extends PSParserBaseListener {
    private AnnotatedTree at;
    private final Stack<Scope> scopeStack = new Stack<>();

    public Pass1TypeAndScopeScanner(AnnotatedTree at) {
        this.at = at;
    }

    private Scope pushScope(Scope scope, ParserRuleContext ctx) {
        at.scopeOfNode.put(ctx, scope);
        scope.setCtx(ctx);

        return scopeStack.push(scope);
    }

    private Scope popScope() {
        return scopeStack.pop();
    }

    private Optional<Scope> currentScope() {
        if (scopeStack.size() > 0) {
            return Optional.ofNullable(scopeStack.peek());
        } else {
            return Optional.empty();
        }
    }

    private Scope currentScope(ParserRuleContext ctx) {
        if (currentScope().isEmpty()) {  // scopeStack is empty or scope is null.
            at.clog("current scope is null!", CompilationLog.LogLevel.ERROR, ctx);
            throw new SemanticFatalError();
        }

        return currentScope().get();
    }

    @Override
    public void enterProg(PSParserParser.ProgContext ctx) {
        NameSpace ns = new NameSpace("", null, ctx);
        at.nameSpace = ns;  // scope的根
        pushScope(ns, ctx);
    }

    @Override
    public void exitProg(PSParserParser.ProgContext ctx) {
        popScope();
    }

    @Override
    public void enterBlock(PSParserParser.BlockContext ctx) {
        // 对于函数，不需要再额外建一个scope
        if (!(ctx.parent instanceof PSParserParser.FunctionBodyContext)) {
            BlockScope blockScope = new BlockScope(currentScope(ctx), ctx);
            currentScope(ctx).addSymbol(blockScope);
            pushScope(blockScope, ctx);
        }
    }

    @Override
    public void exitBlock(PSParserParser.BlockContext ctx) {
        if (!(ctx.parent instanceof PSParserParser.FunctionBodyContext)) {
            popScope();
        }
    }

    @Override
    public void enterStatement(PSParserParser.StatementContext ctx) {
        if (ctx.FOR() != null) {  // 为For建立单独Scope
            BlockScope blockScope = new BlockScope(currentScope(ctx), ctx);
            currentScope(ctx).addSymbol(blockScope);

            pushScope(blockScope, ctx);
        }
    }

    @Override
    public void exitStatement(PSParserParser.StatementContext ctx) {
        if (ctx.FOR() != null) {
            popScope();
        }
    }

    @Override
    public void enterFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx) {
        String idName = ctx.Identifier().getText();

        // function 参数的确定放到 TypeResolver.java
        Function function = new Function(idName, currentScope(ctx), ctx);
        at.types.add(function);

        currentScope(ctx).addSymbol(function);
        pushScope(function, ctx);
    }

    @Override
    public void exitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx) {
        popScope();
    }

    @Override
    public void enterClassDeclaration(PSParserParser.ClassDeclarationContext ctx) {
        String idName = ctx.Identifier().getText();

        Class theClass = new Class(idName, ctx);
        at.types.add(theClass);

        if (at.lookupClass(currentScope(ctx), idName) != null) {
            at.clog("duplicate class name: " + idName, ctx);  // 只是报警，继续解析
        }

        currentScope(ctx).addSymbol(theClass);
        pushScope(theClass, ctx);
    }

    @Override
    public void exitClassDeclaration(PSParserParser.ClassDeclarationContext ctx) {
        popScope();
    }
}
