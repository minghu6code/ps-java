package org.minghu6.ps.interpreter.semantics;

import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.minghu6.ps.interpreter.data.scopes.BlockScope;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Super;
import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.This;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.*;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
/**
 * 第三遍扫描：解析变量、函数、类成员的引用（引用消解），以及类型推断。<br/>
 * 两件事一起做的原因是：
 * <div>
 *     <p>(1) 对于变量，只有引用消解才能推断类型；</p>
 *     <p>(2) 对于函数调用，只有把参数的类型推断出来，才能匹配到正确的函数（方法也是函数）；</p>
 *     <p>(3) 要推导表达式类型，必须知道其中可能含有的函数调用的返回值。</p>
 * </div>
 */
public class Pass3RefResolver extends PSParserBaseListener {
    private final AnnotatedTree at;

    // 把本地变量加入符号表，并计算类型
    ParseTreeWalker walker = new ParseTreeWalker();
    Pass2TypeResolver localVariableEnter;

    // this(), super()构造函数留到最后去消解，因为它可能引用别的构造函数，要等这些构造函数都消解完
    private final List<PSParserParser.FunctionCallContext>
            thisConstructorList = new LinkedList<>();
    private final List<PSParserParser.FunctionCallContext>
            superConstructorList = new LinkedList<>();


    public Pass3RefResolver(AnnotatedTree at) {
        this.at = at;
        localVariableEnter = new Pass2TypeResolver(at, true);
    }

    /**
     * 把本地变量添加到符号表。<br/>
     * 本地变量必须边添加，边解析，不能先添加后解析，否则会引起引用消解的错误。
     */
    @Override
    public void enterVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx) {
        Scope scope = at.enclosingScopeOfNode(ctx);

        if (scope instanceof BlockScope || scope instanceof Function) {
            walker.walk(localVariableEnter, ctx);
        }
    }

    @Override
    public void exitPrimary(PSParserParser.PrimaryContext ctx) {
        Scope scope = at.enclosingScopeOfNode(ctx);
        Type type = null;

        // 标识符
        if (ctx.Identifier() != null) {
            String idName = ctx.Identifier().getText();
            Variable variable = at.lookupVariable(scope, idName);
            if (variable == null) {
                // 看看是不是函数，因为函数可以作为值传递。这时候无法区分重名函数。
                // 普通Scope中的函数不可以重名。
                // 查找函数的时候可能会把类的方法包含进去。
                Function function = at.lookupFunction(scope, idName);
                if (function != null) {
                    at.symbolOfNode.put(ctx, function);
                    type = function;
                } else {
                    at.clog(format("Unknown variable or function: %s", idName), ctx);
                }
            } else {
                at.symbolOfNode.put(ctx, variable);
                type = variable.type;
            }
        }

        // 字面值
        else if (ctx.literal() != null) {
            type = at.typeOfNode.get(ctx.literal());
        }

        // 括号里的表达式
        else if (ctx.expression() != null) {
            type = at.typeOfNode.get(ctx.expression());
        }

        // this 关键字
        else if (ctx.THIS() != null) {
            // 查找Class类型的上级Scope
            Class theClass = at.enclosingClassOfNode(ctx);
            if (theClass != null) {
                This variable = theClass.getThis();
                at.symbolOfNode.put(ctx, variable);
                type = theClass;
            } else {
                at.clog("keyword `THIS` can only be used inside a class", ctx);
            }
        }

        // super 关键字
        else if (ctx.SUPER() != null) {
            // 查找Class类型的上级Scope
            Class theClass = at.enclosingClassOfNode(ctx);
            if (theClass != null) {
                Super variable = theClass.getSuper();
                at.symbolOfNode.put(ctx, variable);
                type = theClass;
            } else {
                at.clog("keyword `SUPER` can only be used inside a class", ctx);
            }
        }

        // 类型推断，冒泡
        at.typeOfNode.put(ctx, type);
    }

    @Override
    public void exitFunctionCall(PSParserParser.FunctionCallContext ctx) {
        // this
        if(ctx.THIS() != null){
            thisConstructorList.add(ctx);
            return;
        }
        // super
        else if(ctx.SUPER() != null){
            superConstructorList.add(ctx);
            return;
        }

        // TODO 临时代码，支持println
        if(ctx.Identifier().getText().equals("println")){
            return;
        }

        String idName = ctx.Identifier().getText();
        // 获得参数类型，这些类型已经在表达式中推断出来
        List<Type> paramTypes = getParamTypes(ctx);

        if (ctx.parent instanceof PSParserParser.ExpressionContext) {
            PSParserParser.ExpressionContext e
                    = (PSParserParser.ExpressionContext) ctx.parent;
            // bop: Binary Operator
            // 处理点号： 类方法调用
            if (e.bop != null && e.bop.getType() == PSParserParser.DOT) {
                handleMethodCall(ctx);
                return;
            }
        }

        Scope scope = at.enclosingScopeOfNode(ctx);

        // 从当前Scope逐级查找函数（包括方法）
        Function function = at.lookupFunction(scope, idName, paramTypes);
        if (function != null){  // Handle trivial function call
            at.symbolOfNode.put(ctx, function);
            at.typeOfNode.put(ctx, function.returnType);
            return;
        }

        // 看看是不是类的构造函数，用相同的名称查找一个class
        Class theClass = at.lookupClass(scope, idName);
        if (theClass != null) {  // Handle class constructor call
            function = theClass.findConstructor(paramTypes);

            if (function != null) {
                at.symbolOfNode.put(ctx, function);
            }

            // 如果是类同名方法，并且没有参数，那么就是缺省构造方法。
            else if (ctx.expressionList() == null) {
                at.symbolOfNode.put(ctx, theClass.defaultConstructor());
            }

            else {
                at.clog("Unknown class constructor: " + ctx.getText(), ctx);
            }

            at.typeOfNode.put(ctx, theClass);  // 这次函数调用是返回一个对象
        }

        // 看看是不是函数型变量
        else {  // Handle function variable call
            Variable variable = at.lookupFVariable(scope,idName, paramTypes);
            if (variable != null && variable.type instanceof FunctionType){
                at.symbolOfNode.put(ctx, variable);
                at.typeOfNode.put(ctx, variable.type);
            }
            else {
                at.clog("Unknown function or function variable: " + ctx.getText(), ctx);
            }
        }
    }

    /**
     * 消解处理点符号表达式的层层引用
     */
    @Override
    public void exitExpression(PSParserParser.ExpressionContext ctx) {
        Type type = null;

        if (ctx.bop != null && ctx.bop.getType() == PSParserParser.DOT) {
            type = resolveDot(ctx);
        }

        // 变量引用冒泡：如果下级是一个变量，往上冒泡传递，以便在点符号表达式中使用
        // （也包括this, super的冒泡）
        else if (ctx.primary() != null) {
            Symbol symbol = at.symbolOfNode.get(ctx.primary());
            at.symbolOfNode.put(ctx, symbol);
        }

        // 类型推断和综合
        if (ctx.primary() != null) {
            type = at.typeOfNode.get(ctx.primary());
        } else if (ctx.functionCall() != null) {
            type = at.typeOfNode.get(ctx.functionCall());
        } else if (ctx.bop != null && ctx.expression().size() >= 2) {
            type = resolveBop(ctx);
        }

        // 类型冒泡
        at.typeOfNode.put(ctx, type);
    }

    // 对变量初始化部分也做一下类型推断
    @Override
    public void exitVariableInitializer(PSParserParser.VariableInitializerContext ctx) {
        if (ctx.expression() != null) {
            at.typeOfNode.put(ctx, at.typeOfNode.get(ctx.expression()));
        }
    }

    // 根据字面值推断类型
    @Override
    public void exitLiteral(PSParserParser.LiteralContext ctx) {
        if (ctx.BooleanLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.Boolean);
        } else if (ctx.CharacterLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.Char);
        } else if (ctx.NullLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.Null);
        } else if (ctx.StringLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.String);
        } else if (ctx.IntegerLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.Integer);
        } else if (ctx.FloatingPointLiteral() != null) {
            at.typeOfNode.put(ctx, PrimitiveType.Float);
        }
    }

    /**
     * 在结束扫描之前，把this和super构造函数消解掉
     */
    @Override
    public void exitProg(PSParserParser.ProgContext ctx) {
        thisConstructorList.forEach(this::resolveThisConstructorCall);
        superConstructorList.forEach(this::resolveSuperConstructorCall);
    }

    /**
     * 获得函数参数列表
     */
    private List<Type> getParamTypes(PSParserParser.FunctionCallContext ctx) {
        if (ctx.expressionList() == null) {
            return new LinkedList<>();
        }
        return ctx.expressionList().expression()
                .stream()
                .map(e -> at.typeOfNode.get(e))
                .collect(Collectors.toList());
    }
    /**
     * Handle method call.
     */
    private void handleMethodCall(PSParserParser.FunctionCallContext ctx) {
        List<Type> paramTypes = getParamTypes(ctx);
        String idName = ctx.Identifier().getText();
        PSParserParser.ExpressionContext e
                = (PSParserParser.ExpressionContext) ctx.parent;

        Symbol symbol = at.symbolOfNode.get(e.expression(0));
        if (symbol instanceof Variable && ((Variable) symbol).type instanceof Class) {
            Class theClass = (Class) ((Variable) symbol).type;
            // 查找名称和参数类型都匹配的函数。
            // 不允许名称和参数都相同，但返回值不同的情况。
            Function function = theClass.getFunction(idName, paramTypes);
            if (function != null) {
                at.symbolOfNode.put(ctx, function);
                at.typeOfNode.put(ctx, function.getReturnType());
            } else {
                Variable funcVar = theClass.getFVariable(idName,paramTypes);
                if (funcVar != null) {
                    at.symbolOfNode.put(ctx, funcVar);
                    at.typeOfNode.put(ctx, ((FunctionType)funcVar.type).getReturnType());
                } else {
                    at.clog(format("Unable to find method %s in Class %s", idName, theClass.getName()), e);
                }
            }
        }
    }

    private Type resolveDot(PSParserParser.ExpressionContext ctx) {
        Type type = null;
        // 这是个左递归，要不断把左边的节点的计算结果存到node2Symbol，所以要在exitExpression里操作
        Symbol symbol = at.symbolOfNode.get(ctx.expression(0));
        if (symbol instanceof Variable && ((Variable) symbol).type instanceof Class) {
            Class theClass = (Class) ((Variable)symbol).getType();

            // 引用类的属性
            if (ctx.Identifier() != null) {
                String idName = ctx.Identifier().getText();
                // 在类的scope里去查找，不需要改变当前scope
                Variable variable = at.lookupVariable(theClass, idName);

                if (variable != null) {
                    at.symbolOfNode.put(ctx, variable);
                    type = variable.type;  // 类型综合 （冒泡）
                } else {
                    at.clog(
                            format("Unable to find field %s in Class %s",
                                    idName, theClass.getName()),
                            ctx
                    );
                }
            }

            // 引用类的方法
            else if (ctx.functionCall() != null) {
                type = at.typeOfNode.get(ctx.functionCall());
            }
        } else {
            at.clog("symbol isn't a qualified object: " + symbol, ctx);
        }

        return type;
    }

    private Type resolveBop(PSParserParser.ExpressionContext ctx) {
        Type type = null;
        Type type1 = at.typeOfNode.get(ctx.expression(0));
        Type type2 = at.typeOfNode.get(ctx.expression(1));

        switch (ctx.bop.getType()) {
            case PSParserParser.ADD:
                if (type1 == PrimitiveType.String || type2 ==  PrimitiveType.String){
                    type = PrimitiveType.String;
                }
                else if (type1 instanceof PrimitiveType && type2 instanceof PrimitiveType){
                    //类型“向上”对齐，比如一个int和一个float，取float
                    type = PrimitiveType.getUpperType(
                            (PrimitiveType) type1,
                            (PrimitiveType) type2
                    );
                }else{
                    at.clog("operand should be PrimitiveType for additive and multiplicative operation", ctx);
                }
                break;
            case PSParserParser.SUB:
            case PSParserParser.MUL:
            case PSParserParser.DIV:
                if (type1 instanceof PrimitiveType && type2 instanceof PrimitiveType){
                    //类型“向上”对齐，比如一个int和一个float，取float
                    type = PrimitiveType.getUpperType(
                            (PrimitiveType) type1,
                            (PrimitiveType) type2
                    );
                }else{
                    at.clog("operand should be PrimitiveType for additive and multiplicative operation", ctx);
                }

                break;
            case PSParserParser.EQUAL:
            case PSParserParser.NOTEQUAL:
            case PSParserParser.LE:
            case PSParserParser.LT:
            case PSParserParser.GE:
            case PSParserParser.GT:
            case PSParserParser.AND:
            case PSParserParser.OR:
            case PSParserParser.BANG:
                type = PrimitiveType.Boolean;
                break;
            case PSParserParser.ASSIGN:
            case PSParserParser.ADD_ASSIGN:
            case PSParserParser.SUB_ASSIGN:
            case PSParserParser.MUL_ASSIGN:
            case PSParserParser.DIV_ASSIGN:
            case PSParserParser.AND_ASSIGN:
            case PSParserParser.OR_ASSIGN:
            case PSParserParser.XOR_ASSIGN:
            case PSParserParser.MOD_ASSIGN:
            case PSParserParser.LSHIFT_ASSIGN:
            case PSParserParser.RSHIFT_ASSIGN:
            case PSParserParser.URSHIFT_ASSIGN:
                type = type1;
                break;
        }

        return type;
    }

    /**
     * 消解 this 构造函数
     */
    private void resolveThisConstructorCall(PSParserParser.FunctionCallContext ctx) {
        Class theClass = at.enclosingClassOfNode(ctx);
        if (theClass != null) {
            Function function = at.enclosingFunctionOfNode(ctx);

            if (function != null && function.isConstructor()){
                //检查是不是构造函数中的第一句
                PSParserParser.FunctionDeclarationContext fdx
                        = (PSParserParser.FunctionDeclarationContext) function.getCtx();
                if (firstStatementInFunction(fdx, ctx)){
                    at.clog("this must be first statement in a constructor", ctx);
                    return;
                }

                List<Type> paramTypes = getParamTypes(ctx);
                Function refered = theClass.findConstructor(paramTypes);
                if (refered != null){
                    at.symbolOfNode.put(ctx,refered);
                    at.typeOfNode.put(ctx,theClass);
                    at.thisConstructorRef.put(function, refered);
                }
                else if (paramTypes.size() == 0){  // 缺省构造函数
                    at.symbolOfNode.put(ctx,theClass.defaultConstructor());
                    at.typeOfNode.put(ctx,theClass);
                    at.thisConstructorRef.put(function, theClass.defaultConstructor());
                }
                else{
                    at.clog("Can't find a constructor matches this()", ctx);
                }
            }
            else{
                at.clog("this() should only be called inside a class constructor", ctx);
            }
        }
        else{
            at.clog("this() should only be called inside a class", ctx);
        }
    }

    /**
     * 消解 super 函数
     */
    private void resolveSuperConstructorCall(PSParserParser.FunctionCallContext ctx) {
        Class theClass = at.enclosingClassOfNode(ctx);

        if (theClass != null) {
            Function function = at.enclosingFunctionOfNode(ctx);

            if (function != null && function.isConstructor()) {
                Class parentClass = theClass.getParentClass();

                if (parentClass != null) {
                    // 检查是不是构造函数的第一句
                    PSParserParser.FunctionDeclarationContext fdx
                            = (PSParserParser.FunctionDeclarationContext) function.getCtx();
                    if (firstStatementInFunction(fdx, ctx)) {
                        at.clog("super must be first statement in a constructor", ctx);
                        return;
                    }

                    List<Type> paramTypes = getParamTypes(ctx);
                    Function refered = parentClass.findConstructor(paramTypes);

                    if (refered != null) {
                        at.symbolOfNode.put(ctx, refered);
                        at.typeOfNode.put(ctx, theClass);
                        at.superConstructorRef.put(function, refered);
                    }
                    else if (paramTypes.size() == 0) {  // 缺省构造函数
                        at.symbolOfNode.put(ctx, parentClass.defaultConstructor());
                        at.typeOfNode.put(ctx,theClass);
                        at.superConstructorRef.put(function, theClass.defaultConstructor());
                    }
                    else {
                        at.clog("Can't find a constructor matches this", ctx);
                    }
                }

                // TODO 父类是最顶层的基类
            }
            else {
                at.clog("Super should only be called inside a class constructor", ctx);
            }
        }
        else {
            at.clog("Super should only be called inside a class", ctx);
        }
    }

    private boolean firstStatementInFunction(
            PSParserParser.FunctionDeclarationContext fdx,
            PSParserParser.FunctionCallContext ctx
    ) {
        PSParserParser.StatementContext statement
                = fdx.functionBody()
                .block()
                .blockStatements()
                .blockStatement(0)
                .statement();
        return statement == null
                || statement.expression() == null
                || statement.expression().functionCall() != ctx;
    }


}
