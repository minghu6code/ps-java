package org.minghu6.ps.interpreter.semantics;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.apache.commons.collections4.SetUtils;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.Function;
import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Pass6ClosureAnalyzer extends PSParserBaseListener {
    AnnotatedTree at = null;

    public Pass6ClosureAnalyzer(AnnotatedTree at) {
        this.at = at;
    }

    /**
     * 对所有平凡函数做闭包分析
     */
    public void analyzeClosures() {
        at.types.stream()
                .filter(type -> type instanceof Function
                        && !((Function) type).isMethod())
                .forEach(type -> ((Function) type).closureVariables = analyzeFunctionClosureVariables((Function) type));
    }

    /**
     * 为某个函数分析闭包变量
     * <div>
     *     算法：
     *     <p> 全部的变量引用 - 内部声明的变量 </p>
     * </div>
     */
    private Set<Variable> analyzeFunctionClosureVariables(Function function) {
        Set<Variable> referenced = referencedVariableOfScope(function);
        Set<Variable> declared = declaredVariableOfScope(function);

        return SetUtils.difference(referenced, declared);
    }
    /**
     * 一个Scope中（包括子scope）声明的所有变量
     */
    private Set<Variable> declaredVariableOfScope(Scope scope) {
        return Stream.concat(
                scope.symbols
                        .stream()
                        .filter(symbol -> symbol instanceof Variable)
                        .map(symbol -> (Variable) symbol),
                scope.symbols
                        .stream()
                        .filter(symbol -> symbol instanceof Scope)
                        .flatMap(symbol -> declaredVariableOfScope((Scope) symbol).stream()))
                .collect(Collectors.toSet());
    }

    /**
     * 一个Scope中所引用的所有变量
     */
    private Set<Variable> referencedVariableOfScope(Scope scope) {
        Stream<Map.Entry<ParserRuleContext, Symbol>> nodeStream;

        // 符号表可能会非常大
        if (at.scopeOfNode.size() > 1000) {
            // 默认使用得是 ForkJoinPool#commonPool()
            nodeStream = at.symbolOfNode.entrySet().parallelStream();
        } else {
            nodeStream = at.symbolOfNode.entrySet().stream();
        }

        return nodeStream
                .filter(entry -> entry.getValue() instanceof Variable
                    && isAncestor(scope.getCtx(), entry.getKey()))
                .map(entry -> (Variable)entry.getValue())
                .collect(Collectors.toSet());
    }

    /**
     * 看看node1是不是node2的祖先
     */
    private boolean isAncestor(RuleContext node1, RuleContext node2) {
        if(node2.parent == null){
            return false;
        } else if(node2.parent == node1){
            return true;
        } else{
            return isAncestor(node1,node2.parent);
        }
    }
}
