package org.minghu6.ps.interpreter.semantics;

import static java.lang.String.format;

import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.AbstractLog;

/**
 * 记录编译过程产生的消息
 */
@SuperBuilder
public class CompilationLog extends AbstractLog {
    @Builder.Default
    final LifeCycle lifeCycle = LifeCycle.COMPILE;

    @Override
    public String toString() {
        return format("[%s] [%s] -- %s @%d:%d", level, lifeCycle, message, ln, col);
    }
}
