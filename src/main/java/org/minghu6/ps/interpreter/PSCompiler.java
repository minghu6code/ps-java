package org.minghu6.ps.interpreter;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.FileUtils;
import org.minghu6.ps.interpreter.runtime.ASTEvaluator;
import org.minghu6.ps.interpreter.semantics.*;
import org.minghu6.ps.interpreter.syntax.PSParserLexer;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class PSCompiler {
    AnnotatedTree at;
    PSParserLexer lexer;
    PSParserParser parser;

    public AnnotatedTree compile(String sourceString, boolean verbose, boolean ast_dump) {
        at = new AnnotatedTree();

        // 词法分析
        lexer = new PSParserLexer(CharStreams.fromString(sourceString));
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // 语法分析
        parser = new PSParserParser(tokens);
        at.ast = parser.prog();

        // 语义分析
        ParseTreeWalker walker = new ParseTreeWalker();

        // 多步的语义分析
        // 允许使用在声明前，便于支持面向对象、递归函数
        Pass1TypeAndScopeScanner pass1TypeAndScopeScanner = new Pass1TypeAndScopeScanner(at);
        walker.walk(pass1TypeAndScopeScanner, at.ast);

        Pass2TypeResolver pass2TypeResolver = new Pass2TypeResolver(at);
        walker.walk(pass2TypeResolver, at.ast);

        Pass3RefResolver pass3RefResolver = new Pass3RefResolver(at);
        walker.walk(pass3RefResolver, at.ast);

        Pass4TypeChecker pass4TypeChecker = new Pass4TypeChecker(at);
        walker.walk(pass4TypeChecker, at.ast);

        Pass5SomeSemanticValidator pass5SomeSemanticValidator
                = new Pass5SomeSemanticValidator(at);
        walker.walk(pass5SomeSemanticValidator, at.ast);

        Pass6ClosureAnalyzer pass6ClosureAnalyzer
                = new Pass6ClosureAnalyzer(at);
        pass6ClosureAnalyzer.analyzeClosures();  // it seems special.

        //打印AST
        if (ast_dump){
            dumpAST();
        }

        //打印符号表
        if(verbose){
            dumpSymbols();
        }

        return at;
    }

    public AnnotatedTree compile(String sourceString) {
        return  compile(sourceString,false, false);
    }

    public AnnotatedTree compile(File sourceFile, boolean symbols_dump, boolean ast_dump) throws IOException {
        String sourceString = FileUtils.readFileToString(sourceFile, StandardCharsets.UTF_8);

        return compile(sourceString, symbols_dump, ast_dump);
    }

    /**
     * 打印符号表
     */
    public void dumpSymbols(){
        if (at != null){
            System.out.println(at.getScopeTreeString());
        }
    }

    /**
     * 打印AST，以lisp格式
     */
    public void dumpAST(){
        if (at!=null) {
            System.out.println(at.ast.toStringTree(parser));
        }
    }

    public void dumpCompilationLogs() {
        // 这应该不会被优化掉吧
        at.dumpCompilationLogs();
    }

    public void dumpRuntimeLogs() {
        at.dumpRuntimeLogs();
    }

    public void dumpLogs() {
        at.dumpLogs();
    }

    public Object Execute(AnnotatedTree at) {
        ASTEvaluator astEvaluator = new ASTEvaluator(at);
        Object result = astEvaluator.visit(at.ast);

        return result;
    }
}
