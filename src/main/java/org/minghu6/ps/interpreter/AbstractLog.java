package org.minghu6.ps.interpreter;

import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.antlr.v4.runtime.ParserRuleContext;

import static java.lang.String.format;

/**
 * 不是Abstract类只是为了实现方便
 */
@SuperBuilder
public abstract class AbstractLog {
    public String message;

    public int ln;

    public int col;

    // 相关的AST节点
    @Builder.Default
    public ParserRuleContext ctx = null;

    @Builder.Default
    public LogLevel level = LogLevel.INFO;

    public LifeCycle lifeCycle;

    public enum LogLevel {
        INFO(0), WARNING(1), ERROR(2);

        LogLevel(int code) {
        }
    }

    /**
     * COMPILE: compile time
     * RUNTIME: runtime
     */
    public enum LifeCycle {
        COMPILE(0), RUNTIME(1);

        LifeCycle(int code) {
        }
    }
}
