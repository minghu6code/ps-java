package org.minghu6.ps.interpreter.data.types;

import lombok.Singular;
import org.minghu6.ps.interpreter.data.scopes.Scope;

public class VoidType implements Type {
    @Override
    public String getName() {
        return "void";
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    private VoidType(){
    }

    //只保留一个实例即可。
    private static final VoidType voidType = new VoidType();

    public static VoidType instance(){
        return voidType;
    }

    @Override
    public boolean isType(Type type){
        return this == type;
    }

    @Override
    public String toString(){
        return "void";
    }
}
