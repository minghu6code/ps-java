package org.minghu6.ps.interpreter.data.types;

public class DefaultConstructor extends Function {
    public DefaultConstructor(String name, Class theClass) {
        super(name, theClass, null);
    }

    public Class Class(){
        return (Class)enclosingScope;
    }
}
