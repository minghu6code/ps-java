package org.minghu6.ps.interpreter.data.values;

import lombok.AccessLevel;
import lombok.Setter;
import org.minghu6.ps.interpreter.data.symbols.Variable;

import java.util.HashMap;
import java.util.Map;

/**
 * PlayScript的对象
 */
public class PlayObject {
    public Map<Variable, Object> fields = new HashMap<>();

    public Object getValue(Variable variable) {
        Object rtn = fields.get(variable);

        if (rtn == null) {
            rtn = NullObject.instance();
        }

        return rtn;
    }

    public void setValue(Variable variable, Object value) {
        fields.put(variable, value);
    }
}

