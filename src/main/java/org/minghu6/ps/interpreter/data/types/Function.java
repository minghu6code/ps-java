package org.minghu6.ps.interpreter.data.types;

import lombok.Getter;
import lombok.Setter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Variable;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class Function extends Scope implements FunctionType {
    public List<Variable> parameters = new LinkedList<>();
    public Type returnType = null;

    public Set<Variable> closureVariables;

    public Function(String name, Scope enclosingScope, ParserRuleContext ctx) {
        this.name = name;
        this.enclosingScope = enclosingScope;
        this.ctx = ctx;
    }

    @Override
    public Type getReturnType() {
        return returnType;
    }

    @Override
    public List<Type> getParamTypes() {
        return parameters
                .stream()
                .map(Variable::getType)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return format("Function %s", name);
    }

    @Override
    public boolean isType(Type type) {
        if (type instanceof FunctionType) {
            return DefaultFunctionType.isType(this, (FunctionType)type);
        }
        return false;
    }

    @Override
    public boolean matchParameterTypes(List<Type> paramTypes) {
        List<Type> thisParamTypes1 = getParamTypes();

        return Stream.of(
                new DefaultFunctionType.FPTParamSizeEquals(),
                new DefaultFunctionType.FPTEachTypeIsType()
        ).allMatch(f -> f.apply(thisParamTypes1, paramTypes));
    }

    /**
     * 检查函数是不是类的方法
     */
    public boolean isMethod() {
        return enclosingScope instanceof Class;
    }

    /**
     * 检查是否是类的构建函数
     */
    public boolean isConstructor() {
        if (this.isMethod()) {
            return enclosingScope.getName().equals(name);
        }
        return false;
    }
}
