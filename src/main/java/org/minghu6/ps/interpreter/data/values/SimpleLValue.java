package org.minghu6.ps.interpreter.data.values;

import lombok.NonNull;
import org.minghu6.ps.interpreter.data.symbols.Super;
import org.minghu6.ps.interpreter.data.symbols.This;
import org.minghu6.ps.interpreter.data.symbols.Variable;

import static java.lang.String.format;

/**
 * 一个简单的左值对象
 */
public class SimpleLValue implements LValue {
    private final Variable variable;

    @NonNull
    private final PlayObject valueContainer;

    public SimpleLValue(Variable variable, PlayObject valueContainer) {
        this.variable = variable;
        this.valueContainer = valueContainer;
    }

    @Override
    public Object getValue() {
        if (variable instanceof This || variable instanceof Super) {
            return valueContainer;
        }

        return valueContainer.getValue(variable);
    }

    @Override
    public void setValue(Object value) {
        valueContainer.setValue(variable, value);

        // 函数型变量还要修改receiver
        if (value instanceof FunctionObject) {
            ((FunctionObject) value).receiver = variable;
        }
    }

    @Override
    public Variable getVariable() {
        return variable;
    }

    @Override
    public String toString() {
        return format("LValue of %s : %s", variable.getName(), getValue());
    }

    @Override
    public PlayObject getValueContainer() {
        return valueContainer;
    }
}
