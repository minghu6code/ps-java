package org.minghu6.ps.interpreter.data.types;

import lombok.AccessLevel;
import lombok.Getter;
import org.minghu6.ps.interpreter.data.scopes.Scope;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DefaultFunctionType implements FunctionType {
    @Getter(value = AccessLevel.PUBLIC)
    public String name = null;

    @Getter(value = AccessLevel.PUBLIC)
    public Scope enclosingScope = null;

    @Getter(value = AccessLevel.PUBLIC)
    public Type returnType = null;

    //对于未命名的类型，自动赋予名字
    private static int nameIndex = 1;

    public DefaultFunctionType (){
        name = "FunctionType" + nameIndex++;
    }

    /**
     * 默认函数的类型
     */
    public List<Type> paramTypes = new LinkedList<>();

    @Override
    public List<Type> getParamTypes() {
        return Collections.unmodifiableList(paramTypes);
    }

    @Override
    public String toString() {
        return "FunctionType";
    }

    @Override
    public boolean isType(Type type) {
        if (type instanceof FunctionType) {
            return isType(this, (FunctionType)type);
        }
        return false;
    }

    public interface FFunTypeEq extends BiFunction<FunctionType, FunctionType, Boolean>{}
    static class FFTTypeEquals implements FFunTypeEq {
        @Override
        public Boolean apply(FunctionType type1, FunctionType type2) {
            return type1 == type2;
        }
    }

    static class FFTReturnTypeIsType implements FFunTypeEq {
        @Override
        public Boolean apply(FunctionType type1, FunctionType type2) {
            return type1.getReturnType().isType(type2.getReturnType());
        }
    }

    static class FFTParamSizeEquals implements FFunTypeEq {
        @Override
        public Boolean apply(FunctionType type1, FunctionType type2) {
            return type1.getParamTypes().size() == type2.getParamTypes().size();
        }
    }

    static class FFTEachParamIsType implements FFunTypeEq {
        @Override
        public Boolean apply(FunctionType type1, FunctionType type2) {
            List<Type> paramTypes1 = type1.getParamTypes();
            List<Type> paramTypes2 = type2.getParamTypes();

            return IntStream.range(0, paramTypes1.size())
                    .allMatch(i -> paramTypes1.get(i).isType(paramTypes2.get(i)));
        }
    }
    /**
     * 工具方法
     * check if function type1 in function type2
     */
    public static boolean isType(FunctionType type1, FunctionType type2) {
        return Stream.of(new FFTTypeEquals())
                .anyMatch(f -> f.apply(type1, type2))  // OR
                    ||
                Stream.of(                             // AND
                        new FFTReturnTypeIsType(),
                        new FFTParamSizeEquals(),
                        new FFTEachParamIsType()
                )
                .allMatch(f -> f.apply(type1, type2));
    }

    public interface FFunParamEq extends BiFunction<List<Type>, List<Type>, Boolean>{}
    static class FPTParamSizeEquals implements FFunParamEq {
        @Override
        public Boolean apply(List<Type> types1, List<Type> types2) {
            return types1.size() == types2.size();
        }
    }
    static class FPTEachTypeIsType implements FFunParamEq {
        @Override
        public Boolean apply(List<Type> types1, List<Type> types2) {
            return IntStream.range(0, types1.size())
                    .allMatch(i -> types1.get(i).isType(types2.get(i)));
        }
    }

    /**
     * 检查函数是否匹配所需参数
     */
    @Override
    public boolean matchParameterTypes(List<Type> paramTypes) {
        return Stream.of(
            new FPTParamSizeEquals(),
            new FPTEachTypeIsType()
        ).allMatch(f -> f.apply(paramTypes, this.paramTypes));
    }
}
