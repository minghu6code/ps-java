package org.minghu6.ps.interpreter.data.values;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.minghu6.ps.interpreter.data.types.Class;

public class ClassObject extends PlayObject {
    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    protected Class type = null;
}
