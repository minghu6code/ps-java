package org.minghu6.ps.interpreter.data.values;

import org.minghu6.ps.interpreter.data.symbols.Variable;

/**
 * 对栈中值的引用
 */
public interface LValue {
    Object getValue();

    void setValue(Object value);

    Variable getVariable();

    PlayObject getValueContainer();
}
