package org.minghu6.ps.interpreter.data.scopes;

import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.data.types.Function;
import org.minghu6.ps.interpreter.data.types.FunctionType;
import org.minghu6.ps.interpreter.data.types.Type;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;

public abstract class Scope extends Symbol {
    // Scope members: variable | method | class | function etc.
    public List<Symbol> symbols = new LinkedList<>();

    public List<Symbol> getSymbols() {
        return Collections.unmodifiableList(this.symbols);
    }

    public boolean containsSymbol(Symbol symbol) {
        return symbols.contains(symbol);
    }
    /**
     * add symbol into scope, and set the symbol enclosingScope point to this
     */
    public void addSymbol(Symbol symbol) {
        symbol.setEnclosingScope(this);
        symbols.add(symbol);
    }

    public static Variable getVariable(Scope scope, String name) {
        return (Variable) scope.symbols
                .stream()
                .filter(s -> s instanceof Variable && s.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    public Variable getVariable(String name) {
        return getVariable(this, name);
    }

    public Function getFunction(String name, List<Type> paramTypes){
        return getFunction(this, name, paramTypes);
    }

    public static Function getFunction(Scope scope, String name, List<Type> paramTypes) {
        return (Function) scope.symbols
                .stream()
                .filter(s -> s instanceof Function
                        && s.getName().equals(name)
                        && ((Function) s).matchParameterTypes(paramTypes))
                .findFirst()
                .orElse(null);
    }

    /**
     * get FunctionType Variable
     */
    public Variable getFVariable(String name, List<Type> paramTypes){
        return getFVariable(this, name, paramTypes);
    }

    public static Variable getFVariable(Scope scope, String name, List<Type> paramTypes) {
        return (Variable) scope.symbols
                .stream()
                .filter(s -> s instanceof Variable
                        && ((Variable) s).getType() instanceof FunctionType
                        && s.getName().equals(name)
                        && ((FunctionType) ((Variable) s).getType()).matchParameterTypes(paramTypes))
                .findFirst()
                .orElse(null);
    }

    public Class getClass(String name){
        return getClass(this,name);
    }

    public static Class getClass(Scope scope, String name){
        for (Symbol s : scope.symbols) {
            if (s instanceof Class && s.getName().equals(name)){
                return (Class) s;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return format("Scope: %s", name);
    }
}
