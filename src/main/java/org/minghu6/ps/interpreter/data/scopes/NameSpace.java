package org.minghu6.ps.interpreter.data.scopes;

import lombok.Getter;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class NameSpace extends BlockScope {
    private NameSpace parent;
    private final List<NameSpace> subNameSpaces = new LinkedList<>();
    @Getter
    private String name;

    public NameSpace(String name, Scope enclosingScope, ParserRuleContext ctx) {
        this.name = name;
        this.enclosingScope = enclosingScope;
        this.ctx = ctx;
    }

    public List<NameSpace> subNameSpaces() {
        return Collections.unmodifiableList(subNameSpaces);
    }

    public void addSubNameSpace(NameSpace child) {
        child.parent = this;
        subNameSpaces.add(child);
    }

    public void removeSubNameSpace(NameSpace child) {
        child.parent = null;
        subNameSpaces.remove(child);
    }
}
