package org.minghu6.ps.interpreter.data.symbols;

import lombok.*;

import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.types.Type;

public class Variable extends Symbol {
    @Getter
    @Setter
    public Type type;

    @Getter
    @Setter
    public Object defaultValue;

    public Variable(String name, Scope enclosingScope, ParserRuleContext ctx) {
        this.name = name;
        this.enclosingScope = enclosingScope;
        this.ctx = ctx;
    }

    public boolean isClassMember() {
        return enclosingScope instanceof Class;
    }

    @Override
    public String toString() {
        return String.format("Variable %s -> %s", name, type);
    }
}
