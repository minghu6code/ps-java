package org.minghu6.ps.interpreter.data.symbols;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.data.scopes.Scope;

/**
 * BaseClass
 */
public abstract class Symbol {

    // symbol name
    @Getter(AccessLevel.PUBLIC)
    @Setter
    protected String name = null;

    // symbol action scope
    @Getter(AccessLevel.PUBLIC)
    @Setter
    protected Scope enclosingScope = null;

    // symbol visibility: public | private
    @Getter(AccessLevel.PUBLIC)
    @Setter
    protected int visibility = 0;

    // symbol related AST node
    @Getter(AccessLevel.PUBLIC)
    @Setter
    protected ParserRuleContext ctx = null;
}
