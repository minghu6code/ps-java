package org.minghu6.ps.interpreter.data.values;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.Function;

/**
 * 存放一个函数运行时的本地变量的值，包括参数的值。
 */
public class FunctionObject extends PlayObject {
    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    protected Function function;

    // 接收者所在的scope。缺省是function的enclosingScope, 也就是词法的Scope。
    // 当赋值给一个函数型类型变量的时候，要修改receiverEnclosingScope。
    @Setter(AccessLevel.PUBLIC)
    @Getter(AccessLevel.PUBLIC)
    protected Variable receiver = null;

    public FunctionObject(Function function) {
        this.function = function;
    }
}
