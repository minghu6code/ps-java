package org.minghu6.ps.interpreter.data.types;

import org.minghu6.ps.interpreter.data.scopes.Scope;

public interface Type {
    public String getName();
    public Scope getEnclosingScope();

    public boolean isType(Type type);
}
