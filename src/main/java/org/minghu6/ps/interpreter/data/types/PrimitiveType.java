package org.minghu6.ps.interpreter.data.types;

import lombok.NoArgsConstructor;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.errors.IncompatiableTypeError;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
public class PrimitiveType implements Type {
    String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Scope getEnclosingScope() {
        return null;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean isType(Type type){
        return this == type;
    }

    // 没有公共的构造方法
    private PrimitiveType(String name) {
        this.name = name;
    }

    // 把常见的基础数据类型都定义出来
    public static PrimitiveType Integer = new PrimitiveType("Integer");
    public static PrimitiveType Long = new PrimitiveType("Long");
    public static PrimitiveType Float = new PrimitiveType("Float");
    public static PrimitiveType Double = new PrimitiveType("Double");
    public static PrimitiveType Boolean = new PrimitiveType("Boolean");
    public static PrimitiveType Byte = new PrimitiveType("Byte");
    public static PrimitiveType Char = new PrimitiveType("Char");
    public static PrimitiveType Short = new PrimitiveType("Short");

    public static PrimitiveType String = new PrimitiveType("String");  // 增加String为基础类型

    public static PrimitiveType Null = new PrimitiveType("null");

    private static final List<PrimitiveType> IntPriority = List.of(
            Boolean,
            Byte,
            Short,
            Integer,
            Long
    );

    private static final List<PrimitiveType> FloatPriority = List.of(
            Float,
            Double
    );

    public static Set<PrimitiveType> NumberType = Stream
            .concat(IntPriority.stream(), FloatPriority.stream())
            .collect(Collectors.toSet());

    public static boolean isIntegerType(PrimitiveType type) {
        return IntPriority.contains(type);
    }

    public static boolean isFloatType(PrimitiveType type) {
        return FloatPriority.contains(type);
    }

    public static boolean isNumberType(PrimitiveType type) {
        return NumberType.contains(type);
    }

//    public static Map<Pair<PrimitiveType, PrimitiveType>, PrimitiveType> UpperTypeMap = Map.ofEntries(
//            new SimpleEntry<>(new Pair<>(String, String), String),
//            new SimpleEntry<>(new Pair<>(Double, Double), Double),
//            new SimpleEntry<>(new Pair<>(Float, Float), Float),
//            new SimpleEntry<>(new Pair<>(Long, Long), Long),
//            new SimpleEntry<>(new Pair<>(Integer, Integer), Integer),
//            new SimpleEntry<>(new Pair<>(Short, Short), Short),
//    );

    public static PrimitiveType getUpperType(Type type1, Type type2)
            throws IncompatiableTypeError {
        if (type1 == Null || type2 == Null) {
            return null;
        }

        try {
            if (isIntegerType((PrimitiveType) type1) && isIntegerType((PrimitiveType) type2)) {
                return IntPriority.get(
                        Math.max(
                                IntPriority.indexOf(type1),
                                IntPriority.indexOf(type2)
                        )
                );
            } else if (isFloatType((PrimitiveType) type1) && isFloatType((PrimitiveType) type2)) {
                return FloatPriority.get(
                        Math.max(
                                FloatPriority.indexOf(type1),
                                FloatPriority.indexOf(type2)
                        )
                );
            } else if (isIntegerType((PrimitiveType) type1) && isFloatType((PrimitiveType) type2)) {
                return (PrimitiveType) type2;
            } else if (isFloatType((PrimitiveType) type1) && isIntegerType((PrimitiveType) type2)) {
                return (PrimitiveType) type1;
            }
        } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
            throw new IncompatiableTypeError();
        }

        throw new IncompatiableTypeError();
    }


}
