package org.minghu6.ps.interpreter.data.types;

import lombok.Getter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Super;
import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.This;
import org.minghu6.ps.interpreter.data.symbols.Variable;

import java.util.List;

public class Class extends Scope implements Type {
    @Getter
    private Class parentClass; //= rootClass;
    private final This thisRef;
    private Super superRef;
    private DefaultConstructor defaultConstructor;

    public Class(String name, ParserRuleContext ctx) {
        this.name = name;
        this.ctx = ctx;

        thisRef = new This(this, ctx);
        thisRef.setType(this);
    }

    public void setParentClass(Class theClass) {
        parentClass = theClass;

        superRef = new Super(parentClass, ctx);
        superRef.setType(parentClass);
    }

    private static final Class rootClass = new Class("Object", null);

    public This getThis(){ return thisRef; }

    public Super getSuper() { return superRef; }

    @Override
    public String toString(){
        return "Class " + name;
    }

    /**
     * 是否包含某个Variable，包括自身以及父类
     */
    @Override
    public Variable getVariable(String name) {
        Variable rtn = super.getVariable(name);

        if (rtn == null && parentClass != null){
            rtn = parentClass.getVariable(name);  //TODO 是否要检查visibility
        }

        return rtn;
    }

    /**
     * 查找构建函数
     */
    public Function findConstructor(List<Type> paramTypes) {
        return super.getFunction(name, paramTypes);  //TODO 是否要检查visibility?
    }

    /**
     * 在自身父类中查找某个方法
     */
    public Function getFunction(String name, List<Type> paramTypes) {
        //在本级查找这个这个方法
        Function rtn = super.getFunction(name, paramTypes);  //TODO 是否要检查visibility?

        //如果在本级找不到，那么递归的从父类中查找
        if (rtn == null && parentClass != null){
            rtn = parentClass.getFunction(name,paramTypes);
        }

        return rtn;
    }

    public Variable getFVariable(String name, List<Type> paramTypes){
        Variable rtn = super.getFVariable(name, paramTypes);  //TODO 是否要检查visibility?

        if (rtn == null && parentClass != null){
            rtn = parentClass.getFVariable(name,paramTypes);
        }

        return rtn;
    }

    @Override
    public boolean containsSymbol(Symbol symbol) {
        if (symbol == thisRef || symbol == superRef) {
            return true;
        }

        boolean rtn = false;
        rtn = symbols.contains(symbol);
        if (!rtn && parentClass != null) {
            rtn = parentClass.containsSymbol(symbol);
        }
        return rtn;
    }

    @Override
    public boolean isType(Type type) {
        if (this == type) return true;

        if (type instanceof Class){
            return ((Class) type).isAncestor(this);
        }
        return false;
    }

    public boolean isAncestor(Class theClass) {
        if (theClass.getParentClass() != null) {
            if (theClass.getParentClass() == this) return true;
            else return isAncestor(theClass.getParentClass());
        }

        return false;
    }

    public DefaultConstructor defaultConstructor() {
        if (defaultConstructor == null) {
            defaultConstructor = new DefaultConstructor(this.name, this);
        }
        return defaultConstructor;
    }
}
