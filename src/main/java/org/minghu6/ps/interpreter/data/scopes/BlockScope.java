package org.minghu6.ps.interpreter.data.scopes;

import org.antlr.v4.runtime.ParserRuleContext;

public class BlockScope extends Scope {
    // 给block编号的数字
    private static int index = 1;

    public BlockScope(){
        this.name = "block" + index++;
    }

    public BlockScope(Scope enclosingScope, ParserRuleContext ctx) {
        this.name = "block" + index++;
        this.enclosingScope = enclosingScope;
        this.ctx = ctx;
    }

    @Override
    public String toString(){
        return "Block " + name;
    }
}
