package org.minghu6.ps.interpreter.data.symbols;

import org.antlr.v4.runtime.ParserRuleContext;

import org.minghu6.ps.interpreter.data.types.Class;

public class This extends Variable {
    public This(Class theClass, ParserRuleContext ctx) {
        super("this", theClass, ctx);
    }

    public Class Class(){
        return (Class) enclosingScope;
    }
}
