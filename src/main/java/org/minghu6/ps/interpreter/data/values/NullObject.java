package org.minghu6.ps.interpreter.data.values;

public final class NullObject extends ClassObject {
    private static final NullObject instance = new NullObject();

    private NullObject() {}

    public static NullObject instance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Null";
    }
}
