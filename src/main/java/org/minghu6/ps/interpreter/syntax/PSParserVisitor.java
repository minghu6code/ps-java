// Generated from /home/minghu6/coding/Java/myworkspaces/PS/src/PSParser.g4 by ANTLR 4.9.1
package org.minghu6.ps.interpreter.syntax;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link PSParserParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface PSParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link PSParserParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(PSParserParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(PSParserParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#blockStatements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatements(PSParserParser.BlockStatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#blockStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(PSParserParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarator(PSParserParser.VariableDeclaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableInitializer(PSParserParser.VariableInitializerContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#typeType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeType(PSParserParser.TypeTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassOrInterfaceType(PSParserParser.ClassOrInterfaceTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionType(PSParserParser.FunctionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#typeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeList(PSParserParser.TypeListContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#functionDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedNameList(PSParserParser.QualifiedNameListContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#typeTypeOrVoid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeTypeOrVoid(PSParserParser.TypeTypeOrVoidContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#formalParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameters(PSParserParser.FormalParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#formalParameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameterList(PSParserParser.FormalParameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#lastFormalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastFormalParameter(PSParserParser.LastFormalParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#variableModifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableModifier(PSParserParser.VariableModifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#formalParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormalParameter(PSParserParser.FormalParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#functionBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionBody(PSParserParser.FunctionBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(PSParserParser.ClassDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#classBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBody(PSParserParser.ClassBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBodyDeclaration(PSParserParser.ClassBodyDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#memberDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMemberDeclaration(PSParserParser.MemberDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldDeclaration(PSParserParser.FieldDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveType(PSParserParser.PrimitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#parExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpression(PSParserParser.ParExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(PSParserParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#forControl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForControl(PSParserParser.ForControlContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#forInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForInit(PSParserParser.ForInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchBlockStatementGroup(PSParserParser.SwitchBlockStatementGroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#switchLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchLabel(PSParserParser.SwitchLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(PSParserParser.PrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(PSParserParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(PSParserParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(PSParserParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link PSParserParser#expressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionList(PSParserParser.ExpressionListContext ctx);
}