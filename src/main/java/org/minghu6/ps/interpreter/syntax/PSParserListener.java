// Generated from /home/minghu6/coding/Java/myworkspaces/PS/src/PSParser.g4 by ANTLR 4.9.1
package org.minghu6.ps.interpreter.syntax;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PSParserParser}.
 */
public interface PSParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PSParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(PSParserParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(PSParserParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(PSParserParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(PSParserParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#blockStatements}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatements(PSParserParser.BlockStatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#blockStatements}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatements(PSParserParser.BlockStatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(PSParserParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(PSParserParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#variableDeclarators}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarator(PSParserParser.VariableDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#variableDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarator(PSParserParser.VariableDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#variableDeclaratorId}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void enterVariableInitializer(PSParserParser.VariableInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void exitVariableInitializer(PSParserParser.VariableInitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#typeType}.
	 * @param ctx the parse tree
	 */
	void enterTypeType(PSParserParser.TypeTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#typeType}.
	 * @param ctx the parse tree
	 */
	void exitTypeType(PSParserParser.TypeTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 */
	void enterClassOrInterfaceType(PSParserParser.ClassOrInterfaceTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#classOrInterfaceType}.
	 * @param ctx the parse tree
	 */
	void exitClassOrInterfaceType(PSParserParser.ClassOrInterfaceTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunctionType(PSParserParser.FunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunctionType(PSParserParser.FunctionTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#typeList}.
	 * @param ctx the parse tree
	 */
	void enterTypeList(PSParserParser.TypeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#typeList}.
	 * @param ctx the parse tree
	 */
	void exitTypeList(PSParserParser.TypeListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedNameList(PSParserParser.QualifiedNameListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#qualifiedNameList}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedNameList(PSParserParser.QualifiedNameListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#typeTypeOrVoid}.
	 * @param ctx the parse tree
	 */
	void enterTypeTypeOrVoid(PSParserParser.TypeTypeOrVoidContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#typeTypeOrVoid}.
	 * @param ctx the parse tree
	 */
	void exitTypeTypeOrVoid(PSParserParser.TypeTypeOrVoidContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameters(PSParserParser.FormalParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameters(PSParserParser.FormalParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameterList(PSParserParser.FormalParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameterList(PSParserParser.FormalParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#lastFormalParameter}.
	 * @param ctx the parse tree
	 */
	void enterLastFormalParameter(PSParserParser.LastFormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#lastFormalParameter}.
	 * @param ctx the parse tree
	 */
	void exitLastFormalParameter(PSParserParser.LastFormalParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#variableModifier}.
	 * @param ctx the parse tree
	 */
	void enterVariableModifier(PSParserParser.VariableModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#variableModifier}.
	 * @param ctx the parse tree
	 */
	void exitVariableModifier(PSParserParser.VariableModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameter(PSParserParser.FormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameter(PSParserParser.FormalParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(PSParserParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(PSParserParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(PSParserParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(PSParserParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(PSParserParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(PSParserParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassBodyDeclaration(PSParserParser.ClassBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassBodyDeclaration(PSParserParser.ClassBodyDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMemberDeclaration(PSParserParser.MemberDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMemberDeclaration(PSParserParser.MemberDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFieldDeclaration(PSParserParser.FieldDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#fieldDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFieldDeclaration(PSParserParser.FieldDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType(PSParserParser.PrimitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType(PSParserParser.PrimitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void enterParExpression(PSParserParser.ParExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void exitParExpression(PSParserParser.ParExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(PSParserParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(PSParserParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#forControl}.
	 * @param ctx the parse tree
	 */
	void enterForControl(PSParserParser.ForControlContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#forControl}.
	 * @param ctx the parse tree
	 */
	void exitForControl(PSParserParser.ForControlContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(PSParserParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(PSParserParser.ForInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 */
	void enterSwitchBlockStatementGroup(PSParserParser.SwitchBlockStatementGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#switchBlockStatementGroup}.
	 * @param ctx the parse tree
	 */
	void exitSwitchBlockStatementGroup(PSParserParser.SwitchBlockStatementGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#switchLabel}.
	 * @param ctx the parse tree
	 */
	void enterSwitchLabel(PSParserParser.SwitchLabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#switchLabel}.
	 * @param ctx the parse tree
	 */
	void exitSwitchLabel(PSParserParser.SwitchLabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(PSParserParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(PSParserParser.PrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(PSParserParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(PSParserParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(PSParserParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(PSParserParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(PSParserParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(PSParserParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link PSParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(PSParserParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PSParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(PSParserParser.ExpressionListContext ctx);
}