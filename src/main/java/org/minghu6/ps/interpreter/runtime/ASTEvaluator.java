package org.minghu6.ps.interpreter.runtime;

import org.minghu6.ps.interpreter.data.scopes.BlockScope;
import org.minghu6.ps.interpreter.data.symbols.Super;
import org.minghu6.ps.interpreter.data.symbols.Symbol;
import org.minghu6.ps.interpreter.data.symbols.This;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.types.*;
import org.minghu6.ps.interpreter.data.types.Class;
import org.minghu6.ps.interpreter.data.values.*;
import org.minghu6.ps.interpreter.errors.IncompatiableTypeError;
import org.minghu6.ps.interpreter.semantics.AnnotatedTree;
import org.minghu6.ps.interpreter.syntax.PSParserBaseVisitor;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

import java.util.*;
import java.util.stream.Collectors;

/**
 * AST 解释器： 利用语义信息
 * {@link org.minghu6.ps.interpreter.semantics.AnnotatedTree}，
 * 在AST上解释执行脚本。
 */
public class ASTEvaluator extends PSParserBaseVisitor<Object> {
    private final AnnotatedTree at;
    private ClassObject classObject;

    public ASTEvaluator(AnnotatedTree at) {
        this.at = at;
    }

    protected boolean traceStackFrame = false;
    protected boolean traceFunctionCall = false;

    /////////////////////////////////////////////////////////////
    /// 栈帧的管理
    private final Stack<StackFrame> stack = new Stack<>();

    /**
     * 栈帧入栈
     * 最重要的任务是保证栈帧的parentFrame设置正确，否则
     * (1) 随着栈的变深，查找变量的性能会降低
     * (2) 甚至有可能找错栈帧，比如在递归（直接）的场景下。
     */
    private void pushStack(StackFrame incomeFrame) {
        if (!stack.empty()) {
            for (int i = stack.size() - 1; i > 0; i--) {
                StackFrame stackFrame = stack.get(i);

                /*
                Sibling

                如果新加入的栈帧，跟某个已有的栈帧的enclosingScope是一样的，那么这俩的parentFrame也一样。
                （因为它们原本就是同一级的）
                比如：
                void foo() {};
                void bar(foo());

                或者：
                void foo();
                if (...) {
                    foo();
                }
                 */
                if (stackFrame.scope.getEnclosingScope() == incomeFrame.scope.getEnclosingScope()) {
                    incomeFrame.parentFrame = stackFrame.parentFrame;
                    break;
                }

                /*
                  Parent

                  如果新加入的帧栈是某个已有的帧栈的下一级，那么就把这个父子关系建立起来。
                  比如：
                  void foo() {
                      if (...) {  // 把这个块往栈桢里加的时候，就符合这个条件。
                      }
                  }
                  再比如，下面的例子：
                  class MyClass {
                      void foo();
                  }
                  MyClass c = MyClass();  // 先加MyClass的
                  c.foo();                // 再加的foo
                 */
                else if (stackFrame.scope == incomeFrame.scope.getEnclosingScope()) {
                   incomeFrame.parentFrame = stackFrame;
                   break;
                }

                /*
                函数运行时的作用域与声明时的作用域不同
                宫：
                对于函数型变量（函数是一等公民）
                在这里设计了一个“receiver”的机制，意思是这个函数是被哪个变量接收了。
                要按照这个receiver的作用域来判断。
                 */
                else if (incomeFrame.object instanceof FunctionObject) {
                    FunctionObject functionObject = (FunctionObject) incomeFrame.object;
                    if (functionObject.getReceiver() != null
                        && functionObject.getReceiver().getEnclosingScope() == stackFrame.scope) {
                        incomeFrame.parentFrame = stackFrame;
                        break;
                    }
                }
            }

            if (incomeFrame.parentFrame == null) {
                incomeFrame.parentFrame = stack.peek();
            }
        }

        stack.push(incomeFrame);

        if (traceStackFrame) {
            dumpStackFrame();
        }
    }

    private StackFrame popStack() {
        return stack.pop();
    }

    private void dumpStackFrame() {
        System.out.println("\nStack Frames ----------------");
        stack.forEach(System.out::println);
        System.out.println("-----------------------------\n");
    }

    public LValue getLValue(Variable variable) {
        StackFrame stackFrame = stack.peek();

        PlayObject valueContainer = null;
        while (stackFrame != null) {
            // 对于对象来说，查找所有父类的属性
            if (stackFrame.scope.containsSymbol(variable)) {
                valueContainer = stackFrame.object;
                break;
            }

            stackFrame = stackFrame.parentFrame;
        }

        // 通过正常的作用域找不到，就从闭包里找
        // 原理：PlayObject中可能有一些变量，其作用域跟StackFrame.scope是不同的。
        if (valueContainer == null) {
            stackFrame = stack.peek();
            while (stackFrame != null) {
                if (stackFrame.contains(variable)) {
                    valueContainer = stackFrame.object;
                    break;
                }

                stackFrame = stackFrame.parentFrame;
            }
        }

        return new SimpleLValue(variable, valueContainer);
    }


    /////////////////////////////////////////////////////////////
    /// 为闭包获取环境变量的值

    /**
     * 为闭包获取环境变量
     * @param function 闭包函数
     * @param valueContainer 存放的环境变量的值
     */
    private void setClosureValues(Function function, PlayObject valueContainer) {
        if (function.closureVariables == null) {
            return;
        }

        Map<Variable, Object> fields = valueContainer.fields;

        fields.putAll(
                function.closureVariables
                .stream()
                .map(var -> new AbstractMap.SimpleEntry<>(var, getLValue(var).getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
        );
    }

    /**
     * 为从函数中返回的对象设置闭包值。<br/>
     * 因为多个函数型属性可能共享值，所以要打包到ClassObject中，而不是functionObject中。
     */
    private void setClosureValues(ClassObject classObject) {
        PlayObject tmpObj = new PlayObject();

        classObject.fields.entrySet()
                .stream()
                .filter(entry -> entry.getKey().getType() instanceof FunctionType
                        && entry.getValue() != null)
                .forEach(entry -> setClosureValues(
                        ((FunctionObject) entry
                                .getValue())
                                .getFunction(),
                        tmpObj));

        classObject.fields.putAll(tmpObj.fields);
    }

    /////////////////////////////////////////////////////////////
    /// 对象初始化

    /**
     * 从父类到子类层层执行缺省的初始化方法（即不带参数的初始化方法）
     */
    public ClassObject createAndInitClassObject(Class theClass) {
        ClassObject obj = new ClassObject();
        obj.setType(theClass);

        Stack<Class> ancestorChain = new Stack<>();

        // 从上到下执行缺省的初始化方法
        ancestorChain.push(theClass);
        while (theClass.getParentClass() != null) {
            ancestorChain.push(theClass.getParentClass());
            theClass = theClass.getParentClass();
        }

        StackFrame frame = new StackFrame(obj);
        pushStack(frame);
        while (!ancestorChain.empty()) { defaultObjectInit(ancestorChain.pop(), obj); }
        popStack();

        return obj;
    }

    // 类的缺省初始化方法
    public void defaultObjectInit(Class theClass, ClassObject obj) {
        Map<Variable, Object> objFields = obj.fields;
        theClass.symbols
                .stream()
                .filter(symbol -> symbol instanceof Variable)
                .forEach(symbol -> objFields.put((Variable) symbol, null));

        //stack.peek().object = obj;

        // 设置了字段的obj怎么能让它们知晓？
        // TODO 其实这里还没干完活。还需要调用显式声明的构造方法

        PSParserParser.ClassBodyContext ctx
                = ((PSParserParser.ClassDeclarationContext)theClass.getCtx()).classBody();

        visitClassBody(ctx);


    }


    /////////////////////////////////////////////////////////////
    /// 内置函数

    /**
     * 硬编码的println
     */
    private void println(PSParserParser.FunctionCallContext ctx) {
        if (ctx.expressionList() != null) {
            Object value = visitExpressionList(ctx.expressionList());
            if (value instanceof LValue) {
                value = ((LValue) value).getValue();
            }
            System.out.println(value);
        } else {
            System.out.println();
        }
    }


    /////////////////////////////////////////////////////////////
    /// 各种运算

    /// 这段儿真是纯搬砖，java这种语法限制又臭又长又累
    private Object add(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.String) {
            rtn = obj1 + String.valueOf(obj2);
        } else if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() + ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() + ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() + ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() + ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() + ((Number) obj2).shortValue();
        }
        else {
            System.out.println("unsupported add operation");
        }

        return rtn;
    }

    private Object minus(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() - ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() - ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() - ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() - ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() - ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Object mul(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() * ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() * ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() * ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() * ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() * ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Object div(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() / ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() / ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() / ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() / ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() / ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Boolean EQ(Object obj1, Object obj2, Type targetType) {
        Boolean rtn;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() == ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() == ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() == ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() == ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() == ((Number) obj2).shortValue();
        }
        //对于对象实例、函数，直接比较对象引用
        else {
            rtn = obj1 == obj2;
        }

        return rtn;
    }

    private Object GE(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() >= ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() >= ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() >= ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() >= ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() >= ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Object GT(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() > ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() > ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() > ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() > ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() > ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Object LE(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() <= ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() <= ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() <= ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() <= ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() <= ((Number) obj2).shortValue();
        }

        return rtn;
    }

    private Object LT(Object obj1, Object obj2, Type targetType) {
        Object rtn = null;
        if (targetType == PrimitiveType.Integer) {
            rtn = ((Number) obj1).intValue() < ((Number) obj2).intValue();
        } else if (targetType == PrimitiveType.Float) {
            rtn = ((Number) obj1).floatValue() < ((Number) obj2).floatValue();
        } else if (targetType == PrimitiveType.Long) {
            rtn = ((Number) obj1).longValue() < ((Number) obj2).longValue();
        } else if (targetType == PrimitiveType.Double) {
            rtn = ((Number) obj1).doubleValue() < ((Number) obj2).doubleValue();
        } else if (targetType == PrimitiveType.Short) {
            rtn = ((Number) obj1).shortValue() < ((Number) obj2).shortValue();
        }

        return rtn;
    }


    /////////////////////////////////////////////////////////////
    /// visit每个节点


    @Override
    public Object visitBlock(PSParserParser.BlockContext ctx) {
        BlockScope scope = (BlockScope) at.scopeOfNode.get(ctx);

        if (scope != null) {  // 有些block是不对应scope的，比如函数底下的block。
            StackFrame stackFrame = new StackFrame(scope);

            pushStack(stackFrame);
        }

        Object rtn = visitBlockStatements(ctx.blockStatements());

        if (scope != null) {
            popStack();
        }

        return rtn;
    }

    @Override
    public Object visitBlockStatement(PSParserParser.BlockStatementContext ctx) {
        Object rtn = null;

        if (ctx.fieldDeclaration() != null) {
            rtn = visitFieldDeclaration(ctx.fieldDeclaration());
        } else if (ctx.statement() != null) {
            rtn = visitStatement(ctx.statement());
        }

        return rtn;
    }

    @Override
    public Object visitFieldDeclaration(PSParserParser.FieldDeclarationContext ctx) {
        Object rtn = null;
        if (ctx.variableDeclarators() != null) {
            rtn = visitVariableDeclarators(ctx.variableDeclarators());
        }
        return rtn;
    }

    @Override
    public Object visitVariableDeclarators(PSParserParser.VariableDeclaratorsContext ctx) {
        Object rtn = null;

        for (PSParserParser.VariableDeclaratorContext child : ctx.variableDeclarator()) {
            rtn = visitVariableDeclarator(child);  // ?
        }
        return rtn;
    }

    @Override
    public Object visitVariableDeclarator(PSParserParser.VariableDeclaratorContext ctx) {
        Object rtn = null;
        LValue lValue = (LValue) visitVariableDeclaratorId(ctx.variableDeclaratorId());
        if (ctx.variableInitializer() != null) {
            rtn = visitVariableInitializer(ctx.variableInitializer());
            if (rtn instanceof LValue) {
                rtn = ((LValue) rtn).getValue();
            }
            lValue.setValue(rtn);
        }
        return rtn;
    }

    // endpoint
    @Override
    public Object visitVariableDeclaratorId(PSParserParser.VariableDeclaratorIdContext ctx) {
        Object rtn;
        Symbol symbol = at.symbolOfNode.get(ctx);
        rtn = getLValue((Variable) symbol);
        return rtn;
    }

    @Override
    public Object visitVariableInitializer(PSParserParser.VariableInitializerContext ctx) {
        Object rtn = null;
        if (ctx.expression() != null) {
            rtn = visitExpression(ctx.expression());
        }
        return rtn;
    }

    @Override
    public Object visitExpression(PSParserParser.ExpressionContext ctx) {
        Object rtn = null;
        if (ctx.bop != null && ctx.expression().size() >= 2) {
            Object left = visitExpression(ctx.expression(0));
            Object right = visitExpression(ctx.expression(1));
            Object leftObject = left;
            Object rightObject = right;

            if (left instanceof LValue) {
                leftObject = ((LValue) left).getValue();
            }

            if (right instanceof LValue) {
                rightObject = ((LValue) right).getValue();
            }

            //本节点期待的数据类型
            Type type = at.typeOfNode.get(ctx);

            //左右两个子节点的类型
            Type type1 = at.typeOfNode.get(ctx.expression(0));
            Type type2 = at.typeOfNode.get(ctx.expression(1));

            try {
                switch (ctx.bop.getType()) {
                    case PSParserParser.ADD:
                        rtn = add(leftObject, rightObject, type);
                        break;
                    case PSParserParser.SUB:
                        rtn = minus(leftObject, rightObject, type);
                        break;
                    case PSParserParser.MUL:
                        rtn = mul(leftObject, rightObject, type);
                        break;
                    case PSParserParser.DIV:
                        rtn = div(leftObject, rightObject, type);
                        break;
                    case PSParserParser.EQUAL:
                        rtn = EQ(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;
                    case PSParserParser.NOTEQUAL:
                        rtn = !EQ(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;
                    case PSParserParser.LE:
                        rtn = LE(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;
                    case PSParserParser.LT:
                        rtn = LT(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;
                    case PSParserParser.GE:
                        rtn = GE(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;
                    case PSParserParser.GT:
                        rtn = GT(leftObject, rightObject, PrimitiveType
                                .getUpperType(type1, type2));
                        break;

                    case PSParserParser.AND:
                        rtn = (Boolean) leftObject && (Boolean) rightObject;
                        break;
                    case PSParserParser.OR:
                        rtn = (Boolean) leftObject || (Boolean) rightObject;
                        break;
                    case PSParserParser.ASSIGN:
                        if (left instanceof LValue) {
                            //((LValue) left).setValue(right);
                            ((LValue) left).setValue(rightObject);
                            rtn = right;
                        } else {
                            System.out.println("Unsupported feature during assignment");
                        }
                        break;

                    default:
                        break;
                }
            } catch (IncompatiableTypeError e) {
                at.rlog("IncompatiableTypeError", ctx);
                dumpStackFrame();
                at.dumpLogs();
                throw new IncompatiableTypeError();
            }

        } else if (ctx.bop != null && ctx.bop.getType() == PSParserParser.DOT) {
            // 此语法是左递归的，算法体现这一点
            Object leftObject = visitExpression(ctx.expression(0));
            if (leftObject instanceof LValue) {
                Object value = ((LValue) leftObject).getValue();
                if (value instanceof ClassObject) {
                    ClassObject valueContainer = (ClassObject) value;
                    Variable leftVar = (Variable)at.symbolOfNode.get(ctx.expression(0));
                    // 获得field或调用方法
                    if (ctx.Identifier() != null) {
                        Variable variable = (Variable) at.symbolOfNode.get(ctx);

                        //对于this和super引用的属性，不用考虑重载，因为它们的解析是准确的
                        if (!(leftVar instanceof This || leftVar instanceof Super)) {
                            //类的成员可能需要重载
                            variable = at.lookupVariable(
                                    valueContainer.getType(),
                                    variable.getName()
                            );
                        }
                        rtn = new SimpleLValue(variable, valueContainer);
                    } else if (ctx.functionCall() != null) {
                        //要先计算方法的参数，才能加对象的StackFrame.
                        if (traceFunctionCall){
                            System.out.println("\n>>MethodCall : " + ctx.getText());
                        }

                        rtn = methodCall(valueContainer, ctx.functionCall(), (leftVar instanceof Super));
                    }
                }
            } else {
                System.out.println("Expecting an Object Reference");
            }

        }

        else if (ctx.primary() != null) {
            rtn = visitPrimary(ctx.primary());
        }

        // 后缀运算，例如：i++ 或 i--
        else if (ctx.postfix != null) {
            Object value = visitExpression(ctx.expression(0));
            LValue lValue = null;
            Type type = at.typeOfNode.get(ctx.expression(0));
            if (value instanceof LValue) {
                lValue = (LValue) value;
                value = lValue.getValue();
            }
            switch (ctx.postfix.getType()) {
                case PSParserParser.INC:
                    if (type == PrimitiveType.Integer) {
                        lValue.setValue((Integer) value + 1);
                    } else {
                        lValue.setValue((Long) value + 1);
                    }
                    rtn = value;
                    break;
                case PSParserParser.DEC:
                    if (type == PrimitiveType.Integer) {
                        lValue.setValue((Integer) value - 1);
                    } else {
                        lValue.setValue((long) value - 1);
                    }
                    rtn = value;
                    break;
                default:
                    break;
            }
        }

        //前缀操作，例如：++i 或 --i
        else if (ctx.prefix != null) {
            Object value = visitExpression(ctx.expression(0));
            LValue lValue = null;
            Type type = at.typeOfNode.get(ctx.expression(0));
            if (value instanceof LValue) {
                lValue = (LValue) value;
                value = lValue.getValue();
            }
            switch (ctx.prefix.getType()) {
                case PSParserParser.INC:
                    if (type == PrimitiveType.Integer) {
                        rtn = (Integer) value + 1;
                    } else {
                        rtn = (Long) value + 1;
                    }
                    lValue.setValue(rtn);
                    break;
                case PSParserParser.DEC:
                    if (type == PrimitiveType.Integer) {
                        rtn = (Integer) value - 1;
                    } else {
                        rtn = (Long) value - 1;
                    }
                    lValue.setValue(rtn);
                    break;
                //!符号，逻辑非运算
                case PSParserParser.BANG:
                    rtn = !((Boolean) value);
                    break;
                default:
                    break;
            }
        } else if (ctx.functionCall() != null) {// functionCall
            rtn = visitFunctionCall(ctx.functionCall());
        }
        return rtn;
    }

    @Override
    public Object visitExpressionList(PSParserParser.ExpressionListContext ctx) {
        Object rtn = null;
        for (PSParserParser.ExpressionContext child : ctx.expression()) {
            rtn = visitExpression(child);
        }
        return rtn;
    }

    @Override
    public Object visitStatement(PSParserParser.StatementContext ctx){
        Object rtn = null;
        if (ctx.statementExpression!= null) {
            rtn = visitExpression(ctx.statementExpression);
        } else if (ctx.IF() != null) {
            Boolean condition = (Boolean) visitParExpression(ctx.parExpression());
            if (Boolean.TRUE == condition) {
                rtn = visitStatement(ctx.statement(0));
            } else if (ctx.ELSE() != null) {
                rtn = visitStatement(ctx.statement(1));
            }
        }

        //while循环
        else if (ctx.WHILE() != null) {
            if (ctx.parExpression().expression() != null && ctx.statement(0) != null) {

                while (true) {
                    //每次循环都要计算一下循环条件
                    Boolean condition;
                    Object value = visitExpression(ctx.parExpression().expression());
                    if (value instanceof LValue) {
                        condition = (Boolean) ((LValue) value).getValue();
                    } else {
                        condition = (Boolean) value;
                    }

                    if (condition) {
                        //执行while后面的语句
                        if (condition) {
                            rtn = visitStatement(ctx.statement(0));

                            //break
                            if (rtn instanceof BreakObject){
                                rtn = null;  //清除BreakObject，也就是只跳出一层循环
                                break;
                            }
                            //return
                            else if (rtn instanceof ReturnObject){
                                break;
                            }
                        }
                    }
                    else{
                        break;
                    }
                }
            }

        }

        //for循环
        else if (ctx.FOR() != null) {
            // 添加StackFrame
            BlockScope scope = (BlockScope) at.scopeOfNode.get(ctx);
            StackFrame frame = new StackFrame(scope);
            // frame.parentFrame = stack.peek();
            pushStack(frame);

            PSParserParser.ForControlContext forControl = ctx.forControl();
            if (forControl.variableDeclaratorId() != null) {
                // forEnhanced for (var : vars)
            } else {
                // 初始化部分执行一次
                if (forControl.forInit() != null) {
                    rtn = visitForInit(forControl.forInit());
                }

                while (true) {
                    Boolean condition = true; // 如果没有条件判断部分，意味着一直循环
                    if (forControl.expression() != null) {
                        Object value = visitExpression(forControl.expression());
                        if (value instanceof LValue) {
                            condition = (Boolean) ((LValue) value).getValue();
                        } else {
                            condition = (Boolean) value;
                        }
                    }

                    if (condition) {
                        // 执行for的语句体
                        rtn = visitStatement(ctx.statement(0));

                        //处理break
                        if (rtn instanceof BreakObject){
                            rtn = null;
                            break;
                        }
                        //return
                        else if (rtn instanceof ReturnObject){
                            break;
                        }

                        // 执行forUpdate，通常是“i++”这样的语句。这个执行顺序不能出错。
                        if (forControl.forUpdate != null) {
                            visitExpressionList(forControl.forUpdate);
                        }
                    } else {
                        break;
                    }
                }
            }

            // 去掉StackFrame
            popStack();
        }

        //block
        else if (ctx.blockLabel != null) {
            rtn = visitBlock(ctx.blockLabel);

        }

        //break语句
        else if (ctx.BREAK() != null) {
            rtn = BreakObject.instance();
        }

        //return语句
        else if (ctx.RETURN() != null) {
            if (ctx.expression() != null) {
                rtn = visitExpression(ctx.expression());

                //return语句应该不需要左值   //TODO 取左值的场景需要优化，目前都是取左值。
                if (rtn instanceof LValue){
                    rtn = ((LValue)rtn).getValue();
                }

                // 把闭包涉及的环境变量都打包带走
                if (rtn instanceof FunctionObject) {
                    FunctionObject functionObject = (FunctionObject) rtn;
                    setClosureValues(functionObject.getFunction(), functionObject);
                }
                //如果返回的是一个对象，那么检查它的所有属性里有没有是闭包的。//TODO 如果属性仍然是一个对象，可能就要向下递归查找了。
                else if (rtn instanceof ClassObject){
                    ClassObject classObject = (ClassObject)rtn;
                    setClosureValues(classObject);
                }

            }

            //把真实的返回值封装在一个ReturnObject对象里，告诉visitBlockStatements停止执行下面的语句
            rtn = new ReturnObject(rtn);
        }
        return rtn;
    }

    @Override
    public Object visitForInit(PSParserParser.ForInitContext ctx) {
        Object rtn = null;
        if (ctx.variableDeclarators() != null) {
            rtn = visitVariableDeclarators(ctx.variableDeclarators());
        } else if (ctx.expressionList() != null) {
            rtn = visitExpressionList(ctx.expressionList());
        }
        return rtn;
    }

    @Override
    public Object visitLiteral(PSParserParser.LiteralContext ctx) {
        Object rtn = null;

        //整数
        if (ctx.IntegerLiteral() != null) {
            if (ctx.IntegerLiteral() != null) {
                rtn = Integer.valueOf(ctx.IntegerLiteral().getText());
            }
        }

        //浮点数
        else if (ctx.FloatingPointLiteral() != null) {
            rtn = Float.valueOf(ctx.getText());
        }

        //布尔值
        else if (ctx.BooleanLiteral() != null) {
            if (ctx.BooleanLiteral().getText().equals("true")) {
                rtn = Boolean.TRUE;
            } else {
                rtn = Boolean.FALSE;
            }
        }

        //字符串
        else if (ctx.StringLiteral() != null) {
            String withQuotationMark = ctx.StringLiteral().getText();
            rtn = withQuotationMark.substring(1, withQuotationMark.length() - 1);
        }

        //单个的字符
        else if (ctx.CharacterLiteral() != null) {
            rtn = ctx.CharacterLiteral().getText().charAt(0);
        }

        //null字面量
        else if (ctx.NullLiteral() != null) {
            rtn = NullObject.instance();
        }

        return rtn;
    }

    @Override
    public Object visitParExpression(PSParserParser.ParExpressionContext ctx) {
        return visitExpression(ctx.expression());
    }

    @Override
    public Object visitPrimary(PSParserParser.PrimaryContext ctx) {
        Object rtn = null;
        //字面量
        if (ctx.literal() != null) {
            rtn = visitLiteral(ctx.literal());
        }
        //变量
        else if (ctx.Identifier() != null) {
            Symbol symbol = at.symbolOfNode.get(ctx);
            if (symbol instanceof Variable) {
                rtn = getLValue((Variable) symbol);
            } else if (symbol instanceof Function) {
                rtn = new FunctionObject((Function) symbol);
            }
        }
        //括号括起来的表达式
        else if (ctx.expression() != null){
            rtn = visitExpression(ctx.expression());
        }
        //this
        else if (ctx.THIS() != null){
            This thisRef = (This)at.symbolOfNode.get(ctx);
            rtn = getLValue(thisRef);
        }
        //super
        else if (ctx.SUPER() != null){
            Super superRef = (Super) at.symbolOfNode.get(ctx);
            rtn = getLValue(superRef);
        }

        return rtn;
    }

    @Override
    public Object visitPrimitiveType(PSParserParser.PrimitiveTypeContext ctx) {
        Object rtn = null;
        if (ctx.INT() != null) {
            rtn = PSParserParser.INT;
        } else if (ctx.LONG() != null) {
            rtn = PSParserParser.LONG;
        } else if (ctx.FLOAT() != null) {
            rtn = PSParserParser.FLOAT;
        } else if (ctx.DOUBLE() != null) {
            rtn = PSParserParser.DOUBLE;
        } else if (ctx.BOOLEAN() != null) {
            rtn = PSParserParser.BOOLEAN;
        } else if (ctx.CHAR() != null) {
            rtn = PSParserParser.CHAR;
        } else if (ctx.SHORT() != null) {
            rtn = PSParserParser.SHORT;
        } else if (ctx.BYTE() != null) {
            rtn = PSParserParser.BYTE;
        }
        return rtn;
    }

    @Override
    public Object visitTypeType(PSParserParser.TypeTypeContext ctx) {
        return visitPrimitiveType(ctx.primitiveType());
    }

    @Override
    public Object visitBlockStatements(PSParserParser.BlockStatementsContext ctx) {
        Object rtn = null;
        for (PSParserParser.BlockStatementContext child : ctx.blockStatement()) {
            rtn = visitBlockStatement(child);

            //如果返回的是break，那么不执行下面的语句
            if (rtn instanceof BreakObject){
                break;
            }

            //碰到Return, 退出函数
            // TODO 要能层层退出一个个block，弹出一个栈桢
            else if (rtn instanceof ReturnObject){
                break;
            }
        }
        return rtn;
    }

    @Override
    public Object visitProg(PSParserParser.ProgContext ctx) {
        Object rtn;
        if (ctx.blockStatements() != null) {
            pushStack(new StackFrame((BlockScope) at.scopeOfNode.get(ctx)));
            rtn = visitBlockStatements(ctx.blockStatements());
            popStack();
        } else {
            rtn = visitBlock(ctx.block());
        }

        return rtn;
    }

    @Override
    public Object visitFunctionCall(PSParserParser.FunctionCallContext ctx) {
        //this
        if (ctx.THIS() != null){
            thisConstructor(ctx);
            return null;  //不需要有返回值，因为本身就是在构造方法里调用的。
        }
        //super
        else if (ctx.SUPER() != null){
            thisConstructor(ctx); //似乎跟this完全一样。因为方法的绑定是解析准确了的。
            return null;
        }

        //if (ctx.IDENTIFIER() == null) return null;  //暂时不支持this和super

        Object rtn = null;

        String functionName = ctx.Identifier().getText();  //这是调用时的名称，不一定是真正的函数名，还可能是函数尅性的变量名

        //如果调用的是类的缺省构造函数，则直接创建对象并返回
        Symbol symbol = at.symbolOfNode.get(ctx);
        //if (symbol instanceof Class) {
        if (symbol instanceof DefaultConstructor) {
            //类的缺省构造函数。没有一个具体函数跟它关联，只是指向了一个类。
            //return createAndInitClassObject((Class) symbol);  //返回新创建的对象
            return createAndInitClassObject(((DefaultConstructor)symbol).Class());  //返回新创建的对象
        }
        //硬编码的一些函数
        else if(functionName.equals("println")){
            // TODO 临时代码，用于打印输出
            println(ctx);
            return null;
        }

        //在上下文中查找出函数，并根据需要创建FunctionObject
        FunctionObject functionObject = getFunctionObject(ctx);
        assert functionObject != null;
        Function function = functionObject.getFunction();

        //如果是对象的构造方法，则按照对象方法调用去执行，并返回所创建出的对象。
        if (function.isConstructor()) {
            Class theClass = (Class) function.getEnclosingScope();

            ClassObject newObject = createAndInitClassObject(theClass);  //先做缺省的初始化

            methodCall(newObject, ctx, false);

            return  newObject;  //返回新创建的对象。
        }

        //计算参数值
        List<Object> paramValues = getParamValues(ctx);

        if (traceFunctionCall){
            System.out.println("\n>>FunctionCall : " + ctx.getText());
        }

        rtn = functionCall(functionObject, paramValues);

        return rtn;
    }

    /**
     * 计算函数调用的参数值
     */
    public List<Object> getParamValues(PSParserParser.FunctionCallContext ctx) {
        if (ctx.expressionList() == null) {
            return new LinkedList<>();
        }

        return ctx.expressionList().expression()
                    .stream()
                    .map(exp -> {
                        Object value = visitExpression(exp);
                        if (value instanceof LValue) {
                            value = ((LValue) value).getValue();
                        }
                        return value;
                    })
                    .collect(Collectors.toList());
    }

    /**
     * 根据函数调用的上下文，返回一个FunctionObject
     * 函数型变量：functionObj存在变量里
     * 普通调用函数：创建一个
     */
    private FunctionObject getFunctionObject(PSParserParser.FunctionCallContext ctx) {
        if (ctx.Identifier() == null) return null;  //暂时不支持this和super

        Function function = null;
        FunctionObject functionObject = null;

        Symbol symbol = at.symbolOfNode.get(ctx);
        //函数类型的变量
        if (symbol instanceof Variable) {
            Variable variable = (Variable) symbol;
            LValue lValue = getLValue(variable);
            Object value = lValue.getValue();
            if (value instanceof FunctionObject) {
                functionObject = (FunctionObject) value;
                function = functionObject.getFunction();
            }
        }
        //普通函数
        else if (symbol instanceof Function) {
            function = (Function) symbol;
        }
        //报错
        else {
            //这是调用时的名称，不一定是真正的函数名，还可能是函数类型的变量名
            String functionName = ctx.Identifier().getText();
            at.clog("unable to find function or function variable " + functionName, ctx);
            return null;
        }

        if (functionObject == null) {
            functionObject = new FunctionObject(function);
        }

        return functionObject;
    }

    /**
     * 执行一个函数的方法体。需要先设置参数值，然后再执行代码。
     */
    private Object functionCall(FunctionObject functionObject, List<Object> paramValues){
        Object rtn;

        //添加函数的栈桢
        StackFrame functionFrame = new StackFrame(functionObject);
        pushStack(functionFrame);

        // 给参数赋值，这些值进入functionFrame
        PSParserParser.FunctionDeclarationContext functionCode
                = (PSParserParser.FunctionDeclarationContext) functionObject.getFunction().getCtx();
        PSParserParser.FormalParameterListContext parameterList
                = functionCode.formalParameters().formalParameterList();

        if (parameterList != null) {
            for (int i = 0; i < parameterList.formalParameter().size(); i++) {
                PSParserParser.FormalParameterContext param
                        = parameterList.formalParameter(i);
                LValue lValue = (LValue) visitVariableDeclaratorId(param.variableDeclaratorId());
                lValue.setValue(paramValues.get(i));
            }
        }

        // 调用函数（方法）体
        rtn = visitFunctionDeclaration(functionCode);

        // 弹出StackFrame
        popStack(); //函数的栈桢

        //如果由一个return语句返回，真实返回值会被封装在一个ReturnObject里。
        if (rtn instanceof ReturnObject){
            rtn = ((ReturnObject)rtn).getReturnValue();
        }

        return rtn;
    }

    /**
     * 对象方法调用。
     * 要先计算完参数的值，然后再添加对象的StackFrame，然后再调用方法。
     * @param classObject  实际调用时的对象。通过这个对象可以获得真实的类，支持多态。
     */
    private Object methodCall(ClassObject classObject,
                              PSParserParser.FunctionCallContext ctx,
                              boolean isSuper) {
        Object rtn;

        //查找函数，并根据需要创建FunctionObject
        //如果查找到的是类的属性，FunctionType型的，需要把在对象的栈桢里查。
        StackFrame classFrame = new StackFrame(classObject);
        pushStack(classFrame);

        FunctionObject functionObject = getFunctionObject(ctx);

        popStack();

        assert functionObject != null;
        Function function = functionObject.getFunction();

        //对普通的类方法，需要在运行时动态绑定
        Class theClass = classObject.getType();   //这是从对象获得的类型，是真实类型。可能是变量声明时的类型的子类
        if (!function.isConstructor() && !isSuper) {
            //从当前类逐级向上查找，找到正确的方法定义
            Function overrides = theClass.getFunction(function.getName(), function.getParamTypes());
            //原来这个function，可能指向一个父类的实现。现在从子类中可能找到重载后的方法，这个时候要绑定到子类的方法上
            if (overrides != null && overrides != function) {
                function = overrides;
                functionObject.setFunction(function);
            }
        }

        //计算参数值
        List<Object> paramValues = getParamValues(ctx);

        //对象的frame要等到函数参数都计算完了才能添加。
        //StackFrame classFrame = new StackFrame(classObject);
        pushStack(classFrame);

        //执行函数
        rtn = functionCall(functionObject, paramValues);

        //弹出栈桢
        popStack();

        return rtn;
    }

    private void thisConstructor(PSParserParser.FunctionCallContext ctx){
        Symbol symbol = at.symbolOfNode.get(ctx);
        //if (symbol instanceof Class){  //缺省构造函数
        if (symbol instanceof DefaultConstructor){  //缺省构造函数
            return; //这里不用管，因为缺省构造函数一定会被调用。
        }
        else if (symbol instanceof Function) {
            Function function = (Function) symbol;
            FunctionObject functionObject = new FunctionObject(function);

            List<Object> paramValues = getParamValues(ctx);

            functionCall(functionObject, paramValues);
        }
    }


    @Override
    public Object visitFunctionDeclaration(PSParserParser.FunctionDeclarationContext ctx) {
        return visitFunctionBody(ctx.functionBody());
    }

    @Override
    public Object visitFunctionBody(PSParserParser.FunctionBodyContext ctx) {
        Object rtn = null;
        if (ctx.block() != null) {
            rtn = visitBlock(ctx.block());
        }
        return rtn;
    }

    @Override
    public Object visitClassBody(PSParserParser.ClassBodyContext ctx) {
        Object rtn = null;
        for (PSParserParser.ClassBodyDeclarationContext child : ctx.classBodyDeclaration()) {
            rtn = visitClassBodyDeclaration(child);
        }
        return rtn;
    }

    @Override
    public Object visitClassBodyDeclaration(PSParserParser.ClassBodyDeclarationContext ctx) {
        Object rtn = null;
        if (ctx.memberDeclaration() != null) {
            rtn = visitMemberDeclaration(ctx.memberDeclaration());
        }
        return rtn;
    }

    @Override
    public Object visitMemberDeclaration(PSParserParser.MemberDeclarationContext ctx) {
        Object rtn = null;
        if (ctx.fieldDeclaration() != null) {
            rtn = visitFieldDeclaration(ctx.fieldDeclaration());
        }
        return rtn;
    }
}
