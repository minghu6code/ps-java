package org.minghu6.ps.interpreter.runtime;

import lombok.Builder;
import lombok.experimental.SuperBuilder;
import org.minghu6.ps.interpreter.AbstractLog;

import static java.lang.String.format;

@SuperBuilder
public class RuntimeLog extends AbstractLog {
    @Builder.Default
    final LifeCycle lifeCycle = LifeCycle.RUNTIME;

    @Override
    public String toString() {
        return format("[%s] [%s] -- %s @%d:%d", level, lifeCycle, message, ln, col);
    }
}
