package org.minghu6.ps.interpreter.runtime;

import org.minghu6.ps.interpreter.data.scopes.BlockScope;
import org.minghu6.ps.interpreter.data.scopes.Scope;
import org.minghu6.ps.interpreter.data.symbols.Variable;
import org.minghu6.ps.interpreter.data.values.ClassObject;
import org.minghu6.ps.interpreter.data.values.FunctionObject;
import org.minghu6.ps.interpreter.data.values.PlayObject;


public class StackFrame {
    // frame <=> scope
    Scope scope;

    // 同一级函数调用，跟上一级的 parentFrame 相同
    // 下一级的函数调用和block的 parentFrame 是自己
    // 闭包 带一个存放在堆里的环境。
    StackFrame parentFrame = null;

    // 实际存放变量
    PlayObject object;

    public boolean contains(Variable variable) {
        if (object != null && object.fields != null) {
            return object.fields.containsKey(variable);
        }
        return false;
    }

    public StackFrame(BlockScope scope) {
        this.scope = scope;
        this.object = new PlayObject();
    }

    public StackFrame(ClassObject object) {
        this.scope = object.getType();
        this.object = object;
    }

    /**
     * 为函数调用创建一个StackFrame
     */
    public StackFrame(FunctionObject object){
        this.scope = object.getFunction();
        this.object = object;
    }

    @Override
    public String toString() {
        return String.format("%s%s", scope,
                parentFrame == null ? "" : " -> " + parentFrame);
    }
}
