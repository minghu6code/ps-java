package org.minghu6.ps.formatter;

import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStreamRewriter;

import org.minghu6.ps.interpreter.syntax.PSParserBaseListener;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

public class TokensRewriter extends PSParserBaseListener {
    BufferedTokenStream tokens;
    TokenStreamRewriter rewriter;

    public TokensRewriter(BufferedTokenStream tokens) {
        this.tokens = tokens;
        this.rewriter = new TokenStreamRewriter(tokens);
    }

    @Override
    public void enterClassDeclaration(PSParserParser.ClassDeclarationContext ctx) {
        if (ctx.Identifier().getText().equals("Mammal")) { // for test, mammal.play
            if (ctx.classBody() != null && ctx.classBody().classBodyDeclaration() != null) {
                ctx.classBody().classBodyDeclaration().forEach(
                        d -> {
                            if (d.memberDeclaration() != null
                                    && d.memberDeclaration().functionDeclaration() != null) {
                                PSParserParser.FunctionDeclarationContext
                                        fd = d.memberDeclaration().functionDeclaration();
                                if (fd.Identifier().getText().equals("speak")
                                        && fd.functionBody().block() != null) {
                                    doRewriteMammalPrint(fd.functionBody().block());
                                }
                            }
                        }
                );
            }
        }
    }

    private void doRewriteMammalPrint(PSParserParser.BlockContext ctx) {
        ctx.blockStatements().blockStatement()
                .stream()
                .filter(blkStmt -> blkStmt.statement() != null)
                .forEach(blkStmt -> {
                            if (blkStmt.statement().statementExpression != null
                                    && blkStmt.statement().statementExpression.functionCall() != null) {
                                PSParserParser.FunctionCallContext
                                        funCall = blkStmt.statement().statementExpression.functionCall();
                                if (funCall.Identifier() != null
                                        && funCall.Identifier().getText().equals("println")) {
                                    funCall.expressionList().expression().forEach(ex -> {
                                        if (ex.primary() != null
                                                && ex.primary().literal() != null
                                                && ex.primary().literal().StringLiteral() != null
                                        ) {
                                            Token originStrLit = ex.primary().literal().StringLiteral().getSymbol();
                                            String newStr = "\"I have cracked!\"";

                                            rewriter.replace(originStrLit, newStr);
                                        }
                                    });
                                }
                            }
                        }
                );
    }
}
