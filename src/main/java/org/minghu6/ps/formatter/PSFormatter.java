package org.minghu6.ps.formatter;


import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.minghu6.ps.interpreter.syntax.PSParserLexer;
import org.minghu6.ps.interpreter.syntax.PSParserParser;

public class PSFormatter {
    String sourceString;

    public PSFormatter(String sourceString) {
        this.sourceString = sourceString;
    }

    public void fmt() {
        // 词法分析
        PSParserLexer lexer = new PSParserLexer(CharStreams.fromString(sourceString));
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // 语法分析
        PSParserParser parser = new PSParserParser(tokens);
        ParseTree ast = parser.prog();

        // 语义分析
        ParseTreeWalker walker = new ParseTreeWalker();
        TokensRewriter tokensRewriter = new TokensRewriter(tokens);

        walker.walk(tokensRewriter, ast);

        System.out.println(tokensRewriter.rewriter.getText());
    }
}
