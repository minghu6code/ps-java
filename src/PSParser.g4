//上游根原件： https://github.com/antlr/grammars-v4/blob/master/java/JavaParser.g4

/*
 [The "BSD licence"]
 Copyright (c) 2013 Terence Parr, Sam Harwell
 Copyright (c) 2017 Ivan Kochurkin (upgrade to Java 8)
 Copyright (c) 2019 Richard Gong
 Copyright (c) 2021 John Zhuang
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

grammar PSParser;
import PSLexer;  // 导入词法定义

/*下面的内容加到所生成的Java源文件的头部，如包名称，import语句等。*/
//@header {
//package org.minghu6.ps.interpreter;
//}


/*
 * 递归下降算法
*/

// Entrypoint
prog
    : blockStatements
    | block
    ;

block
    : LBRACE blockStatements RBRACE
    ;

blockStatements
    : blockStatement*
    ;

blockStatement
    : statement
    | fieldDeclaration
    | functionDeclaration
    | classDeclaration
    ;

variableDeclarators
    : typeType variableDeclarator (COMMA variableDeclarator)*
    ;

variableDeclarator
    : variableDeclaratorId (ASSIGN variableInitializer)?
    ;

variableDeclaratorId
    : Identifier (LBRACK RBRACK)*
    ;

variableInitializer
    : LBRACE (variableInitializer (COMMA variableInitializer)* COMMA?)? RBRACE
    | expression
    ;

typeType
    : (classOrInterfaceType | functionType | primitiveType) (LBRACK RBRACK)*
    ;

classOrInterfaceType
    : Identifier (DOT Identifier)*
    ;

functionType
    : FUNCTION typeTypeOrVoid LPAREN typeList? RPAREN
    ;

typeList
    : typeType (COMMA typeType)*
    ;

functionDeclaration
    : typeTypeOrVoid? Identifier formalParameters (LBRACK RBRACK)*
      (THROWS qualifiedNameList)?
      functionBody
    ;

qualifiedNameList
    : classOrInterfaceType (COMMA classOrInterfaceType)*
    ;

typeTypeOrVoid
    : typeType
    | VOID
    ;

formalParameters
    : LPAREN formalParameterList? RPAREN
    ;

formalParameterList
    : formalParameter (COMMA formalParameter)* (COMMA lastFormalParameter)?
    | lastFormalParameter
    ;

lastFormalParameter
    : variableModifier* typeType ELLIPSIS variableDeclaratorId
    ;

variableModifier
    : FINAL
    ;

formalParameter
    : variableModifier* typeType variableDeclaratorId
    ;

functionBody
    : block
    | SEMI
    ;

classDeclaration
    : CLASS Identifier (EXTENDS typeType)? (IMPLEMENTS typeList)? classBody
    ;

classBody
    : LBRACE classBodyDeclaration* RBRACE
    ;

classBodyDeclaration
    : SEMI
    | memberDeclaration
    ;

memberDeclaration
    : functionDeclaration
    | fieldDeclaration
    | classDeclaration
    ;

fieldDeclaration
    : variableDeclarators SEMI
    ;

primitiveType
    : BOOLEAN
    | CHAR
    | BYTE
    | SHORT
    | INT
    | LONG
    | FLOAT
    | DOUBLE
    | STRING
    ;

parExpression
    : LPAREN expression RPAREN
    ;

statement
    : blockLabel=block
    | IF parExpression statement (ELSE statement)?
    | FOR LPAREN forControl RPAREN statement
    | WHILE parExpression statement
    | DO statement WHILE parExpression SEMI
    | SWITCH parExpression LBRACE switchBlockStatementGroup* switchLabel* RBRACE
    | RETURN expression? SEMI
    | BREAK Identifier? SEMI
    | CONTINUE Identifier SEMI
    | SEMI
    | statementExpression=expression SEMI
    | identifierLabel=Identifier COLON statement
    ;

forControl
    : typeType variableDeclaratorId COLON expression
    | forInit? SEMI expression SEMI forUpdate=expressionList?
    ;

forInit
    : variableDeclarators
    | expressionList
    ;

switchBlockStatementGroup
    : switchLabel+ blockStatement+
    ;

switchLabel
    : CASE (constantExpression=expression | enumContantName=Identifier) COLON
    | DEFAULT COLON
    ;

primary
    : LPAREN expression RPAREN
    | THIS
    | SUPER
    | literal
    | Identifier
    ;

literal
    : IntegerLiteral
    | FloatingPointLiteral
    | BooleanLiteral
    | CharacterLiteral
    | StringLiteral
    | NullLiteral
    ;

expression
    : primary
    | expression bop=DOT  // binary operator
      ( Identifier
      | functionCall
      | THIS
      )
    | expression LBRACK expression RBRACK
    | functionCall
    | expression postfix=(INC | DEC)
    | prefix=(ADD | SUB | INC | DEC) expression
    | prefix=(TILDE | BANG) expression
    | expression bop=(MUL | DIV | MOD) expression
    | expression bop=(ADD | SUB) expression  // 以规则的先后顺序作为优先级
    | expression bop=(LSHIFT | URSHIFT | RSHIFT)
    | expression bop=(LE | GE | GT | LT) expression
    | expression bop=INSTANCEOF typeType
    | expression bop=(EQUAL | NOTEQUAL) expression
    | expression bop=BITAND expression
    | expression bop=CARET expression
    | expression bop=BITOR expression
    | expression bop=AND expression
    | expression bop=OR expression
    | expression bop=QUESTION expression COLON expression
    | <assoc=right> expression
      //bop=assignmentOperator
      bop=(ASSIGN
           | ADD_ASSIGN
           | SUB_ASSIGN
           | MUL_ASSIGN
           | DIV_ASSIGN
           | AND_ASSIGN
           | OR_ASSIGN
           | XOR_ASSIGN
           | RSHIFT_ASSIGN
           | URSHIFT_ASSIGN
           | LSHIFT_ASSIGN
           | MOD_ASSIGN
          )
      expression
    ;

functionCall
    : Identifier LPAREN expressionList? RPAREN
    | THIS LPAREN expressionList? RPAREN
    | SUPER LPAREN expressionList? RPAREN
    ;

expressionList
    : expression (COMMA expression)*
    ;

//assignmentOperator
//    : ASSIGN
//    | MUL_ASSIGN
//    | DIV_ASSIGN
//    | MOD_ASSIGN
//    | ADD_ASSIGN
//    | SUB_ASSIGN
//    | AND_ASSIGN
//    | XOR_ASSIGN
//    | OR_ASSIGN
//    | LSHIFT_ASSIGN
//    | RSHIFT_ASSIGN
//    | URSHIFT_ASSIGN
//    ;
