import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import org.javatuples.Pair;
import org.junit.jupiter.api.*;
import org.minghu6.ps.interpreter.PSCompiler;
import org.minghu6.ps.interpreter.runtime.ASTEvaluator;
import org.minghu6.ps.interpreter.runtime.RuntimeLog;
import org.minghu6.ps.interpreter.semantics.AnnotatedTree;
import org.minghu6.ps.interpreter.semantics.CompilationLog;
import static org.minghu6.ps.interpreter.AbstractLog.LogLevel;

import java.io.IOException;
import java.nio.file.*;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.List;
import static java.lang.String.format;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RunExamples {
    private List<Path> exampleScripts;
    private final FileSystem fs = FileSystems.getDefault();
    private final Path examplesDir = Paths.get(System.getProperty("resourcesDir"));
    private final Set<String> shouldFailedFileNames = Set.of(
            "semantic-check.play"
    );
    private final LinkedHashMap<Path, AnnotatedTree> pathAT = new LinkedHashMap<>();
    private final LinkedHashMap<Path, Pair<Object, Object>> pathResult = new LinkedHashMap<>();

    private void prepareFiles() {
        try (Stream<Path> walk = Files.walk(examplesDir)) {
            // We want to find only regular files
            System.out.println("\nPrepared files:\n");
            this.exampleScripts = walk
                    .filter(Files::isRegularFile)
                    .peek(System.out::println)
                    .filter(file -> fs.getPathMatcher("glob:*.play").matches(file.getFileName()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RunExamples() {
        prepareFiles();
    }

    @Test
    @Order(1)
    public void compile() {
        this.pathAT.clear();

        exampleScripts
                .stream()
                .filter(path -> !shouldFailedFileNames.contains(path.toFile().getName()))
                .forEach(this::compileFile);
    }

    private void compileFile(Path scriptPath) {
        PSCompiler compiler = new PSCompiler();
        AnnotatedTree at;
        try {
            at = compiler.compile(scriptPath.toFile(), false, false);
        } catch (Exception e) {
            assertThat(e).describedAs(
                    String.join("\n",
                            format("Failed script file: %s", scriptPath),
                            e.toString()))
                    .isEqualTo(null);
            return;
        }

        assertThat(at.compilationLogs)
                .as("there should be no error log in compilation.")
                .withFailMessage(
                        Stream.concat(
                            Stream.of(format("Compile Failed script file: %s", scriptPath)),
                            at.compilationLogs.stream().map(CompilationLog::toString)
                        ).collect(Collectors.joining("\n"))
                )
                .allMatch(log -> log.level != LogLevel.ERROR);

        pathAT.put(scriptPath, at);
    }

    @Test
    @Order(2)
    public void evaluate() {
        this.pathResult.clear();

        exampleScripts
                .stream()
                .filter(path -> !shouldFailedFileNames.contains(path.toFile().getName()))
                .forEach(this::evaluateAT);

        this.pathAT.clear();
    }

    private void evaluateAT(Path scriptPath) {
        AnnotatedTree at = pathAT.get(scriptPath);
        ASTEvaluator evaluator = new ASTEvaluator(at);
        Object result = null;
        try {
            result = evaluator.visit(at.ast);
        } catch (Exception e) {
            assertThat(e).describedAs(
                    String.join("\n",
                            format("Run Failed script file: %s", scriptPath),
                            e.toString()))
                    .isEqualTo(null);

        }

        assertThat(at.compilationLogs)
                .as("there should be no error log in runtime.")
                .withFailMessage(
                        Stream.concat(
                                Stream.of(format("Run Failed script file: %s", scriptPath)),
                                at.runtimeLogs.stream().map(RuntimeLog::toString)
                        ).collect(Collectors.joining("\n"))
                )
                .allMatch(log -> log.level != LogLevel.ERROR);

        //pathResult.put(scriptPath, result);
    }
}
