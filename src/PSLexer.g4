/*
 [The "BSD licence"]
 Copyright (c) 2013 Terence Parr, Sam Harwell
 Copyright (c) 2017 Ivan Kochurkin (upgrade to Java 8)
 Copyright (c) 2019 Richard Gong
 Copyright (c) 2021 John Zhuang
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

lexer grammar PSLexer;

// channels { COMMENTS, WHITESPACES }  // channels { COMMENTS, WHITESPACES }

// 关键字
BOOLEAN : 'boolean';
BREAK : 'break';
BYTE : 'byte';
CASE : 'case';
CATCH : 'catch';
CHAR : 'char';
CLASS : 'class';
CONST : 'const';
CONTINUE : 'continue';
DEFAULT : 'default';
DO : 'do';
DOUBLE : 'double';
ELSE : 'else';
ENUM : 'enum';
EXTENDS : 'extends';
FINAL : 'final';
FINALLY : 'finally';
FLOAT : 'float';
FOR : 'for';
IF : 'if';
IMPLEMENTS : 'implements';
IMPORT : 'import';
INSTANCEOF : 'instanceof';
INT : 'int';
INTERFACE : 'interface';
LONG : 'long';
NATIVE : 'native';
NEW : 'new';
PACKAGE : 'package';
PRIVATE : 'private';
PROTECTED : 'protected';
PUBLIC : 'public';
RETURN : 'return';
SHORT : 'short';
SUPER : 'super';
SWITCH : 'switch';
STRING : 'string';
THIS : 'this';
VOID : 'void';
WHILE : 'while';
FUNCTION : 'function';
THROWS : 'throws';

// 整形数字面值
IntegerLiteral
    : DecimalIntegerLiteral  // 10进制整型字面值
    | HexIntegerLiteral      // 16进制
    | OctalIntegerLiteral    // 8进制
    | BinaryIntegerLiteral   // 2进制
    ;

fragment
DecimalIntegerLiteral
    : DecimalNumeral IntegerTypeSuffix?
    ;

fragment
HexIntegerLiteral
    : HexNumeral IntegerTypeSuffix?
    ;

fragment
OctalIntegerLiteral
    : OctalNumeral IntegerTypeSuffix?
    ;

fragment
BinaryIntegerLiteral
    : BinaryNumeral IntegerTypeSuffix?
    ;

fragment
IntegerTypeSuffix
    : [lL]  // 5l or 5L
    ;

/**
 *  DecimalNumeral 10进制整型字面值
 */
fragment
DecimalNumeral  // 0, 20_1, 212
    : '0'
    | NonZeroDigit (Digits? | Underscores Digits)
    ;
fragment
Digits  // 0_1, 22, 3, 4__5
    : Digit (((Digit | UNDERSCORE)+)? Digit)?
    ;

fragment
Digit
    : '0'
    | NonZeroDigit
    ;

fragment
NonZeroDigit  // 这种用宏来做就很省代码
    : [1-9]
    ;

DigitsAndUnderscores
    : (Digit | UNDERSCORE)+
    ;

fragment
Underscores
    : UNDERSCORE+
    ;

fragment
HexNumeral
    : '0' [xX] HexDigits
    ;

fragment
HexDigits
    : HexDigit (((HexDigit | UNDERSCORE)+)? HexDigit)?
    ;

fragment
HexDigit
    : [0-9a-fA-F]
    ;

fragment
OctalNumeral
    : '0' Underscores? OctalDigits
    ;

fragment
OctalDigits
    : OctalDigit ((OctalDigit | UNDERSCORE)+? OctalDigit)?
    // .+?x 实际上起到了(.+(?=x))不可取代的作用，因为x可以匹配进去了
    // ? 匹配尽可能少，而不是默认的贪心匹配
    ;

fragment
OctalDigit
    : [0-7]
    ;

fragment
BinaryNumeral
    : '0' [bB] BinaryDigits
    ;

fragment
BinaryDigits
    : BinaryDigit ((BinaryDigit | UNDERSCORE)+? BinaryDigit)?
    ;

fragment
BinaryDigit
    : [01]
    ;

// 浮点数字面值
FloatingPointLiteral
    : DecimalFloatingPointLiteral  // 10进制浮点数
    | HexadecimalFloatingPointLiteral  // 16进制浮点数
    ;

fragment
DecimalFloatingPointLiteral
    : Digits DOT Digits? ExponentPart? FloatTypeSuffix?  // 5.4, 5.2e, 5.2ef (float), 5.2eD(double)
    | DOT Digits ExponentPart? FloatTypeSuffix?
    | Digits ExponentPart FloatTypeSuffix?
    | Digits FloatTypeSuffix
    ;

fragment
ExponentPart
    : ExponentIndicator SignedInteger
    ;

fragment
ExponentIndicator
    : [eE]
    ;

fragment
SignedInteger
    : Sign? Digits
    ;

fragment
Sign
    : [+-]
    ;

fragment
FloatTypeSuffix
    : [fFdD]
    ;

fragment
HexadecimalFloatingPointLiteral
    : HexSignificand BinaryExponent FloatTypeSuffix?
    ;

fragment
HexSignificand
    : HexNumeral '.'?
    | '0' [xX] HexDigits? '.' HexDigits
    ;

fragment
BinaryExponent
    : BinaryExponentIndicator SignedInteger
    ;

fragment
BinaryExponentIndicator
    : [pP]
    ;

// 布尔字面值
BooleanLiteral
    : 'true'
    | 'false'
    ;

// 字符字面值
CharacterLiteral  // 'a', 'b'
    : '\'' SingleCharacter '\''  // 'singlequote SingleCharacter singlequote'
    | '\'' EscapeSequence '\''
    ;

fragment
SingleCharacter
    : ~['\\\r\n]
    ;

// 字符串字面值
StringLiteral
    : '"' (StringCharacter+)? '"'
    ;

fragment
StringCharacter
    : ~["\\\r\n]
    | EscapeSequence
    ;

fragment
EscapeSequence
    : '\\' [btnfr"'\\]
    | OctalEscape  // 允许8进制转义
    | UnicodeEscape // This is not in the spec but prevents having to preprocess the input
    ;

fragment
OctalEscape
    : '\\' OctalDigit
    | '\\' OctalDigit OctalDigit
    | '\\' ZeroToThree OctalDigit OctalDigit
    ;

fragment
ZeroToThree
    : [0-3]
    ;

fragment
UnicodeEscape
    : '\\' 'u'+ HexDigit HexDigit HexDigit HexDigit
    ;

// null
NullLiteral
    : 'null'
    ;

LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COMMA : ',';
DOT : '.';
ELLIPSIS : '...';
AT : '@';
COLONCOLON : '::';
UNDERSCORE : '_';


ASSIGN : '=';
GT : '>';
LT : '<';
BANG : '!';
TILDE : '~';
QUESTION : '?';
COLON : ':';
ARROW : '->';
EQUAL : '==';
LE : '<=';
GE : '>=';
NOTEQUAL : '!=';
AND : '&&';
OR : '||';
INC : '++';
DEC : '--';
ADD : '+';
SUB : '-';
MUL : '*';
DIV : '/';
BITAND : '&';
BITOR : '|';
CARET : '^';
MOD : '%';
LSHIFT : '<<';
RSHIFT : '>>';
URSHIFT : '>>>';

ADD_ASSIGN : '+=';
SUB_ASSIGN : '-=';
MUL_ASSIGN : '*=';
DIV_ASSIGN : '/=';
AND_ASSIGN : '&=';
OR_ASSIGN : '|=';
XOR_ASSIGN : '^=';
MOD_ASSIGN : '%=';
LSHIFT_ASSIGN : '<<=';
RSHIFT_ASSIGN : '>>=';
URSHIFT_ASSIGN : '>>>=';

Identifier
    : Letter LetterOrDigit*
    ;

fragment
Letter
    : [a-zA-Z$_] // these are the "java letters" below 0x7F = 127
    | // covers all characters above 0x7F which are not a surrogate
     ~[\u0000-\u007F\uD800-\uDBFF]
     {Character.isJavaIdentifierStart(_input.LA(-1))}?
    | // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
     [\uD800-\uDBFF] [\uDC00-\uDFFF]
     {Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
    ;

fragment
LetterOrDigit
    : [a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
    | // covers all characters above 0x7F which are not a surrogate
      ~[\u0000-\u007F\uD800-\uDBFF]
     {Character.isJavaIdentifierPart(_input.LA(-1))}?  // embeded target lang
    | // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
     [\uD800-\uDBFF] [\uDC00-\uDFFF]
     {Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
    ;

// 空白字符 和 注释
WS  :  [ \t\r\n\u000C]+ -> channel(2)
    ;

COMMENT
    :   '/*' .*? '*/' -> channel(1)
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> channel(1)
    ;
