## Dependencies
    1. JDK >= 11
    2. Gradle ^6.8.3

## Build

### gradle run
`gradle run --args 'run examples/BlockScope.play'`

### package fat jar
`gradle jar`

**AndThen**

run `pslang`
    
*rlwrap is optional* 

## Integration Test
`gradle integrationTest --info`